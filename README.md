Screeps doc:
https://docs.screeps.com
https://screeps.fandom.com/wiki/Creep

Grafana integration:
https://screepspl.us/services/grafana/
https://screepsworld.com/2017/10/grafana-graphing-your-ai/
https://github.com/bonzaiferroni/bonzAI/wiki/Screepspl.us-agent-with-Compute-Engine

VSCode:
https://code.visualstudio.com/
https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools

Python (required for emscripten):
https://www.python.org/downloads/

emscripten:
https://emscripten.org/docs/getting_started/downloads.html#sdk-installation-instructions
https://github.com/emscripten-core/emsdk
https://github.com/emscripten-core/emscripten/blob/master/src/settings.js

WASM Optimizations:
https://blog.magnum.graphics/announcements/new-emscripten-application-implementation/
https://floooh.github.io/2016/08/27/asmjs-diet.html

AI diagram:
https://www.draw.io/#G15fox9_AhmHNATWHnBSKgMUISdkMmKK-9

Room plan example:
https://en.wikipedia.org/wiki/Space-filling_tree
http://screeps.dissi.me/#N4IgdghgtgpiBcIAuMAeSCiqAOB7ATkiADQgDOAFhPgCYLlW0AMJI+AxgDb0AcrARgFcAlpxrCwAczIJQ7XGDJJ8g9ihoB1CJ27xQeGfADaoVAgDMTUgE8EARgBMAX2Kn7dm-fMu38BwE5PeDtvVxAzYIcguwAWH3CEByiQW2C4sIiHHmj03xDogFZ4zPNC4sSAdjKMiyqU+yKatOiANnK-GNb2hxaupvNe+uC2prsC6Iru8aG7Sf7p1Nn2u0HFnm6PGfWmh06t7tX7bd8A6OOE+HM9teW6xf9u5Pvu0pmHnbv7d5Pst-bzV7PUa-VIOJiPIJgl6Q8EfGHdEGJWG+cxPJHLQJDBx2DaQnE7a6JfEnQ5+YkXU5Y8kRcybUHUhAxOlE5ZWLHOHZs0EclFcxI8i4xPl+AUROzMvyhPKkhxSi52TGguWZCWy7qEyUHSHKxKKxI6y6qg1XbX-PWa0Zojq3SG5eWI607K27aFYu2ZT6On62-7O90WQGJf2XB0u0aBvyNPKehxR+2QuOZZ2J-UJ7oxlN+UOZ1Fp-oR2P-DPLDU9ZYykZ5c1lnaqysU0v1zIV7rVpsWOv-Rv-FujBaVKaQuYo-t+Yfy0vnMUyqdfSGzsnz9VLnYz1srlGqhcmrHbtfhyHfeUxo9i0On-mH12gi9jq87c+++8ogu38wn5ZWyw2obf4FBP8Ti-ZEKQjQCKU9cDMgdKCLGA-4wJAmkYKQ9wAIZYZ0IxLDaxwk4NVpLVfwwylUkI-oJXIlECIw8xzSo+VhVRbomNFCxWPaeiDQVACDWxXjl1-PjSQBdchP+SjuJg4MkgAmSwJkyCZOkp9f2DWYAMzWTfy0sCtMgrSYJzPTywA9tF1-czdjMoiyKs+jzJ3OzllHd9B1-ccaVczz7AIhdrN-fyRL3ACF3yX9b1lADIsgt9YohIYhQRIIkv6B1UpOCUmTE1JsoAXRcNhoGwagiD0EADAQEwKVHOx-I1BV3KBEloki0tUMjeFVy6oC8WvFkdlHbF0z6h9RsyyE2JFSb+pFQTuSa-lbKWuF2WStadmrKawVNJ1dpOAs+KGviYz40NjWFNVRmrGTLpk1UZNLGShpkmUZJu-47okn1rrzE5Lq01UtNLLShq0mUtOrHMAe+rFMw0rFzLqyErMuqznSsgsrKGqyYys0NHLR1TQUcrHTKxHzghjSnkYp7C6c5IdcQZ3qWdApmCQ5lq2Y9LmKVDSnSIHfpLsp2k+ZpZ0xYLMXSxpmMwtDfzLv851-ILfyhv8hX1tBbcVeJxJtw1+mbxY58KVVNqLcyIbIplSLqzfS63yt032PN38Ov4r35o9waAO9kTvfojrLED2GyO9piSK-EiwJI1ySMgkiYNomPmbI7av22sDtoI7bXO2kTtsg7aYO2+js4EnYwKOmuTkgs6G4bOTFsuV6285nT2-McHNN7toCqcJwgA

TODO:
	- RoomPlanner (distance to best base center point)
	- Use integer room coordinates/signature instead of std::string
	- Various manager (Economy, Defense, Offense, Market, etc.)
	- Setup Grafana Dashboards (Memory.stats? Use remote segment?)
	- Generic "last frame" vs "current frame" information delta
	- Implement Room OwnershipAcquired Event
	- Task/job/trigger system, with dependencies
	- "Forgettable" memory segments (LRU, keep last accessed tick, recompute when necessary)
	- Compression per memory segment

DONE:
	- Calculate walkable terrain cost matrix
	- Proper build system
	- Use global heap for memory cache. Use Memory.Load when global cache is invalidated
	- Setup javascript-side debugging toggle (Memory.debug?)
	- Use polymorphism for the event system (virtual fns)
	- virtualize serialization (read vs write + stream type)
	- Generic events
	- LZW to binary compression
	- Serialization/Deserialization from Memory.raw
	- Native C++ to Javascript bindings