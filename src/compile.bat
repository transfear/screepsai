@echo off

REM Check if emscripten is installed
SET emdskInstallDir=..\emsdk-master\
IF NOT EXIST %emdskInstallDir% GOTO EMSDKNOTINSTALLED

pushd %emdskInstallDir%
call emsdk_env.bat
popd

call BuSy\BuSy.exe BuSy\screepsai.bsproj -build -t Release -v > compile.log 2>&1

GOTO END

:EMSDKNOTINSTALLED
echo ERROR: emsdk not found. Aborting.
EXIT /B 1

:END
