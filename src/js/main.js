'use strict';

const wasm_loader = require('wasm_loader')

/// Module by gameLoop_js.js and gameLoop_asm.wasm files
const mod = wasm_loader('gameLoop_js', 'gameLoop_asm');

module.exports.loop = function ()
{
    delete Memory.creeps;   // Remove automatic memory aliasing for new creeps

	//Memory.raw = '';
    mod.native_loop();
}