#include "Room.h"
#include "ai/object/Room.h"

namespace ai::econ
{
    Room::Room(ai::object::Room* _roomObj)
    : SerializableAggregate(std::forward_as_tuple(_roomObj, _roomObj->GetName()))
    {
    }
}
