#include "Manager.h"
#include "ai/Global.h"
#include "ai/dbg/Assert.h"
#include "ai/object/Room.h"

namespace ai::econ
{
    void Manager::AddRoom(ai::object::Room& _roomObj, ai::Global& _gbl)
    {
        const std::string& roomName = _roomObj.GetName();

        // Save it
        RoomMap& roomMap = m_data.get<ManagerSRL::RoomMap>().get();
        AI_ASSERT(_gbl.dbg, roomMap.find(roomName) == roomMap.end());

        roomMap.emplace_hint(roomMap.end(), roomName, &_roomObj);

        //------------------------------------------------------------------------------------------
        // Goals
        //  1.  Accumulate energy
        //          Conditions:
        //              a. Has free creep of type "worker" -> requires (MOVE + WORK + CARRY)
        //              b. Can store more energy
        //          Actions:
        //              Assign "worker" jobs to creep
        //
        //  2.  Increase gathering speed (spawn worker)
        //          Conditions:
        //              a. Energy acquisition is not saturated
        //              b. Has enough energy for a new worker
        //              c. Has an available spawner
        //              d. No free worker
        //          Actions:
        //              Spawn new worker
        //
        //  3.  Increase energy capacity (spawn extensions)
        //          Conditions:
        //              a. Energy acquisition is saturated (Goal #2a isn't active)
        //              b. RCL level allows more extensions
        //          Actions:
        //              Build a new extension
        //
        //  4.  Free workers (if too many)
        //          Conditions:
        //              a. Cannot accumulate more energy (spawn and extensions are saturated)
        //          Actions:
        //              Free workers
        //
        //  5.  Upgrade RCL
        //          Conditions:
        //              a. Has free creep of type "upgrader" -> requires (MOVE + WORK + CARRY)
        //              b. Energy capacity and gathering speed are saturated (Goal #2a and #3 aren't active)
        //              c. RCL upgrading speed is not saturated
        //              d. RCL can be further upgraded (beware of RCL 8)
        //          Actions:
        //              Assign "upgrader" jobs to creep
        //
        //  6.  Increase RCL upgrading speed (spawn upgraders)
        //          Conditions:
        //              a. RCL upgrading is not saturated
        //              b. Has enough energy for a new upgrader
        //              c. Has an available spawner
        //              d. No free upgrader
        //          Actions:
        //              Spawn new upgrader
        //
        //  7.  Free upgraders (if too many)
        //          Conditions:
        //              a. RCL upgrading is saturated
        //          Actions:
        //              Free upgrader
        //  
        //------------------------------------------------------------------------------------------
        //
        // Conditions:
        //  1. Has free worker/upgrader
        //      Ask CreepManager(?) for free creeps in this room
        //      Must have WORK+CARRY+MOVE parts
        //
        //  2. Can store more energy
        //      Loop on all deposits
        //      Currently accumulated energy + current energy being delivered < capacity
        //
        //  3. Energy acquisition is not saturated
        //      ????
        //
        //  4. Has enough energy for a new worker
        //      Sum all energy in extensions + spawn
        //      Total energy > amount required for WORK+CARRY+MOVE minimal creep
        //
        //  5. No free worker
        //      All creeps in the room have a task assigned
        //
        //  6. Energy acquisition currently saturated --> !(Condition #3)
        //
        //  7. RCL level allows more extensions
        //      See https://docs.screeps.com/api/#StructureExtension
        //      Number of extensions in current room is less than limit
        //
        //  8. RCL isn't maxed
        //      RCL level < 8
        //------------------------------------------------------------------------------------------
        //
        // Hook conditions to events!
        //
        //------------------------------------------------------------------------------------------
        //
        //
        //  Assign "worker" jobs to creep  
        //
        //      Find least saturated route (source <-> deposit)
        //          
        //      Job cycle: Go to source -> Harvest until full -> while !empty (go to closest !full deposit -> Transfer)
        //
        //      Invalidate job:
        //          - if deposit is destroyed (must find a new deposit immediately)
        //          - if a new deposit is build (could be the closest deposit)
        //          - if currently assigned deposit is full
        //      
        //------------------------------------------------------------------------------------------

    }

    void Manager::RemoveRoom(const std::string& _roomName, ai::Global& _gbl)
    {
        // Remove from the saved map
        RoomMap& roomMap = m_data.get<ManagerSRL::RoomMap>().get();
        auto it = roomMap.find(_roomName);
        AI_ASSERT(_gbl.dbg, it != roomMap.end());

        roomMap.erase(it);
    }
}
