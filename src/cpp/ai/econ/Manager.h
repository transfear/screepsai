#pragma once

#include <unordered_map>

#include "ai/serial/SerializableAggregate.h"
#include "ai/serial/SerializableField.h"
#include "ai/utils/TaggedTuple.h"

#include "Room.h"

namespace ai
{
    struct Global;

    namespace object      { class Room; }
    namespace creep::mine { class Creep; }

    namespace econ
    {
        using RoomMap = std::unordered_map<std::string, Room>;
        namespace ManagerSRL
        {
            struct RoomMap {};
            using Data = ai::utils::tagged_tuple::tagged_tuple
            <
                RoomMap, serial::SerializableField<econ::RoomMap>
            >;
        }

        class Manager : public serial::SerializableAggregate<ManagerSRL::Data>
        {
            public:
                inline Manager() { }
                
                void AddRoom(ai::object::Room& _roomObj, ai::Global& _gbl);
                void RemoveRoom(const std::string& _roomName, ai::Global& _gbl);

                void OnCreepAcquired(ai::object::Room* _pRoom, ai::creep::mine::Creep* _pCreep);
        };
    }
}
