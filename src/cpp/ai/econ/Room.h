#pragma once

#include "ai/serial/SerializableAggregate.h"
#include "ai/serial/object/Room.h"
#include "ai/utils/TaggedTuple.h"

namespace ai
{
    namespace object
    {
        class Room;
    }
    
    namespace econ
    {
        namespace RoomSRL
        {
            struct Room {};
            using Data = ai::utils::tagged_tuple::tagged_tuple
            <
                Room, serial::object::Room
            >;
        }

        class Room : public serial::SerializableAggregate<RoomSRL::Data>
        {
            public:
                inline Room() { }
                Room(ai::object::Room* _roomObj);

                inline const ai::object::Room* GetRoomObj() const { return m_data.get<RoomSRL::Room>().getPtr(); }
        };
    }
}
