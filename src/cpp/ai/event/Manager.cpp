#include "Manager.h"

#include <cstdio>

namespace ai::event
{
    void Manager::ProcessEvents(ai::Global& _gbl)
    {
        // Loop as long as we're pushing events
        while (!m_EventLists.empty())
        {
            EventArray localArray;
            localArray.swap(m_EventLists);

            // Process all events
            for (const auto& event : localArray)
            {
                event->Process(_gbl);
            }
        }
    }
}
