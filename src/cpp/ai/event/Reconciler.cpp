#include "Reconciler.h"

#include "NoOp.h"
#include "Manager.h"

#include "ai/GameState.h"
#include "ai/mem/MemoryState.h"

#include "creep/enemy/Appeared.h"
#include "creep/enemy/Disappeared.h"
#include "creep/mine/Acquired.h"
#include "creep/mine/Lost.h"

#include "room/OwnershipAcquired.h"
#include "room/OwernshipLost.h"
#include "room/VisibilityAcquired.h"
#include "room/VisibilityLost.h"

namespace ai::event
{
    void Reconciler::Reconcile()
    {
        ReconcileRooms();
        ReconcileCreeps();

        // TODO add other stuff
    }


    void Reconciler::ReconcileRooms()
    {
        const ai::GameState::RoomMap&              gsRooms  = m_gs.GetRooms();
        const std::vector<mem::MemoryState::Room>& memRooms = m_mem.GetRooms();

        // Make a local copy to work with
        std::vector<RoomData> local_gs_rooms;
        local_gs_rooms.reserve(gsRooms.size());
        std::transform(gsRooms.begin(), gsRooms.end(), std::back_inserter(local_gs_rooms), [](const auto& mapPair)
        {
            const ai::object::Room& gsRoom = mapPair.second;
            return RoomData { &gsRoom.GetName(), &const_cast<ai::object::Room&>(gsRoom), gsRoom.GetFlags(), eSource::kGameState };
        });

        std::vector<RoomData> local_mem_rooms;
        local_mem_rooms.reserve(memRooms.size());
        std::transform(memRooms.begin(), memRooms.end(), std::back_inserter(local_mem_rooms), [](const auto& room)
        {
            return RoomData { &room.GetName(), nullptr, static_cast<object::RoomFlags>(room.GetFlags()), eSource::kMemory };
        });
        
        const auto roomNameSort = [](const RoomData& a, const RoomData& b) { return a.pName->compare(*b.pName) < 0; };
        std::sort(local_gs_rooms.begin(),  local_gs_rooms.end(),  roomNameSort);
        std::sort(local_mem_rooms.begin(), local_mem_rooms.end(), roomNameSort);

        // Find rooms in which visibility has changed
        ReconcileRoomFlag<object::RoomFlags::kVisible, room::VisibilityAcquired, room::VisibilityLost, NoOp>(local_gs_rooms, local_mem_rooms);

        // Find rooms in which ownership has changed
        ReconcileRoomFlag<object::RoomFlags::kOwned, room::OwnershipAcquired, room::OwnershipLost, NoOp>(local_gs_rooms, local_mem_rooms);
        
    }

    template <object::RoomFlags flag, typename EventWhenAcquired, typename EventWhenLost, typename EventWhenUnknown>
    void Reconciler::ReconcileRoomFlag(const std::vector<Reconciler::RoomData>& gs_rooms, const std::vector<RoomData>& mem_rooms)
    {
        const auto roomFlagComparer = [](const RoomData& a, const RoomData& b)
        {
            const int iNameCmp = a.pName->compare(*b.pName);
            if (iNameCmp != 0)
                return iNameCmp < 0;

            const bool aHasFlag = (static_cast<object::RoomFlags>(a.flags) & flag) != object::RoomFlags::kNone;
            const bool bHasFlag = (static_cast<object::RoomFlags>(b.flags) & flag) != object::RoomFlags::kNone;
            return aHasFlag && !bHasFlag;
        };

        // Remove rooms that haven't changed since last tick
        std::vector<RoomData> roomChanged;
        roomChanged.reserve(gs_rooms.size() + mem_rooms.size());
        std::set_symmetric_difference(
            gs_rooms.begin(), gs_rooms.end(),
            mem_rooms.begin(), mem_rooms.end(),
            std::back_inserter(roomChanged), roomFlagComparer
        );

        // Rooms are already sorted by name
        for (auto it = roomChanged.begin(); it != roomChanged.end(); ++it)
        {
            // When have data from both the current (GameState) and previous (Memory) 
            // tick, only keep data from current tick.
            auto curRoom  = it;
            auto nextRoom = it + 1;
            if (nextRoom < roomChanged.end())
            {
                if (nextRoom->pName->compare(*curRoom->pName) == 0)
                {
                    if (nextRoom->source == eSource::kGameState)
                        curRoom = nextRoom;

                    ++it;
                }
            }

           if (curRoom->source == eSource::kGameState)
           {
                if ((static_cast<object::RoomFlags>(curRoom->flags) & flag) != object::RoomFlags::kNone)
                {
                    m_eventMgr.PushEvent(std::make_unique<EventWhenAcquired>(*curRoom->roomPtr));
                }
                else
                {
                    m_eventMgr.PushEvent(std::make_unique<EventWhenLost>(*curRoom->pName));
                }
           }
           else
           {
               // We only have data from previous tick
               if constexpr (!std::is_same<EventWhenUnknown, NoOp>::value)
               {
                   m_eventMgr.PushEvent(std::make_unique<EventWhenUnknown>(*curRoom->pName));
               }
           }
        }
    }

    void Reconciler::ReconcileCreeps()
    {
        const ai::GameState::CreepMap&              gsCreeps  = m_gs.GetCreeps();
        const std::vector<mem::MemoryState::Creep>& memCreeps = m_mem.GetCreeps();

        // Make a local copy to work with
        std::vector<CreepData> local_gs_creeps;
        local_gs_creeps.reserve(gsCreeps.size());
        std::transform(gsCreeps.begin(), gsCreeps.end(), std::back_inserter(local_gs_creeps), [](const auto& mapPair)
        {
            const ai::object::Creep& gsCreep = mapPair.second;
            return CreepData { &gsCreep.GetId(), &const_cast<ai::object::Creep&>(gsCreep), gsCreep.GetFlags(), eSource::kGameState };
        });

        std::vector<CreepData> local_mem_creeps;
        local_mem_creeps.reserve(memCreeps.size());
        std::transform(memCreeps.begin(), memCreeps.end(), std::back_inserter(local_mem_creeps), [](const auto& creep)
        {
            return CreepData { &creep.GetId(), nullptr, static_cast<object::CreepFlags>(creep.GetFlags()), eSource::kMemory };
        });
        
        const auto creepIdSort = [](const CreepData& a, const CreepData& b) { return a.pId->compare(*b.pId) < 0; };
        std::sort(local_gs_creeps.begin(),  local_gs_creeps.end(),  creepIdSort);
        std::sort(local_mem_creeps.begin(), local_mem_creeps.end(), creepIdSort);

        // Remove creeps that haven't changed since last tick
        std::vector<CreepData> creepChanged;
        creepChanged.reserve(gsCreeps.size() + memCreeps.size());
        std::set_symmetric_difference(
            local_gs_creeps.begin(), local_gs_creeps.end(),
            local_mem_creeps.begin(), local_mem_creeps.end(),
            std::back_inserter(creepChanged), creepIdSort
        );

        // Process all changed creeps
        for (const auto& creep : creepChanged)
        {
            if (creep.source == eSource::kGameState)
            {
                // New creep this tick
                if (object::TestFlag<object::CreepFlags::kMine>(creep.flags))
                {
                    m_eventMgr.PushEvent(std::make_unique<creep::mine::Acquired>(*creep.creepPtr));
                }
                else if (object::TestFlag<object::CreepFlags::kEnemy>(creep.flags))
                {
                    m_eventMgr.PushEvent(std::make_unique<creep::enemy::Appeared>(*creep.creepPtr));
                }
            }
            else
            {
                // Creep from previous tick
                if (object::TestFlag<object::CreepFlags::kMine>(creep.flags))
                {
                    m_eventMgr.PushEvent(std::make_unique<creep::mine::Lost>(*creep.pId));
                }
                else if (object::TestFlag<object::CreepFlags::kEnemy>(creep.flags))
                {
                    m_eventMgr.PushEvent(std::make_unique<creep::enemy::Disappeared>(*creep.pId));
                }
            }
        }
    }
}
