#pragma once

#include <memory>
#include <vector>

#include "IEvent.h"

namespace ai::event
{
    class Manager
    {
        public:
            using EventArray = std::vector<std::unique_ptr<IEvent>>;
            
            template <typename EventType>
            inline void PushEvent(std::unique_ptr<EventType> _event)
            {
                m_EventLists.push_back(std::move(_event));
            }

            void ProcessEvents(ai::Global& _gbl);
        
        private:
            EventArray m_EventLists;
    };   
}
