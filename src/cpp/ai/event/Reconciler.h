#pragma once

#include <algorithm>
#include "ai/object/Creep.h"
#include "ai/object/Room.h"
#include "ai/mem/Memory.h"

namespace ai
{
    class GameState;

    namespace event
    {
        class Manager;

        class Reconciler
        {
            public:        
                Reconciler(Manager& _eventMgr, GameState& _gs, const mem::Memory& _mem) : m_eventMgr(_eventMgr), m_gs(_gs), m_mem(_mem.GetPrevState()) { }

                void Reconcile();

            private:

                void ReconcileRooms();
                void ReconcileCreeps();


                enum class eSource : uint8_t
                {
                    kGameState,
                    kMemory
                };

                struct RoomData
                {
                    const std::string* pName;
                    object::Room*      roomPtr;
                    object::RoomFlags  flags;
                    eSource            source;
                };

                template <object::RoomFlags flag, typename EventWhenAcquired, typename EventWhenLost, typename EventWhenUnknown>
                void ReconcileRoomFlag(const std::vector<RoomData>& gs_rooms, const std::vector<RoomData>& mem_rooms);

                struct CreepData
                {
                    const std::string* pId;
                    object::Creep*     creepPtr;
                    object::CreepFlags flags;
                    eSource            source;
                };

                Manager&                m_eventMgr;
                GameState&              m_gs;
                const mem::MemoryState& m_mem;
        };
    }
}
