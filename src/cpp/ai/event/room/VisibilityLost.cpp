#include "VisibilityLost.h"
#include "ai/dbg/Macros.h"
#include "ai/Global.h"

namespace ai::event::room
{
    void VisibilityLost::Process(ai::Global& _gbl) const
    {
        EVT_LOG(_gbl.dbg, "VisibilityLost: %s", m_roomName.c_str());
    }
}
