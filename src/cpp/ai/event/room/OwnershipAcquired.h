#pragma once

#include "ai/event/IEvent.h"

namespace ai
{
    namespace object { class Room; }
    
    namespace event::room
    {
        class OwnershipAcquired : public ai::event::IEvent
        {
            public:
                inline OwnershipAcquired(object::Room& _room) : m_room(_room) { }
                
                virtual void Process(ai::Global& _gbl) const override;

            private:
                object::Room& m_room;
        };
    }
}
