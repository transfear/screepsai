#include "VisibilityAcquired.h"
#include "ai/dbg/Macros.h"
#include "ai/Global.h"
#include "ai/object/Room.h"

namespace ai::event::room
{
    void VisibilityAcquired::Process(ai::Global& _gbl) const
    {
        EVT_LOG(_gbl.dbg, "VisibilityAcquired: %s", m_room.GetName().c_str());
    }
}
