#include "OwnershipAcquired.h"
#include "ai/dbg/Macros.h"
#include "ai/Global.h"

namespace ai::event::room
{
    void OwnershipAcquired::Process(ai::Global& _gbl) const
    {
        EVT_LOG(_gbl.dbg, "OwnershipAcquired: %s", m_room.GetName().c_str());

        // TODO
        //  If we never ran the "FindSources" task on the room, run it.
        //  If the structures information in the room is out of date, update it (alas, start the Update Structures Info task). Need a bit to know if its valid or not.
        //  Evaluate energy harvesting saturation
        //
        //  FIND_SOURCES:
        //      For each energy source
        //          Calculate how many tiles have access to the source
        //
        //  FIND_STRUCTURES:
        //      For each structure
        //          If my spawn, save it
        //          If one of my extension, add it to the list
        //
        //  Evaluate energy harvesting saturation:
        //      For each energy source, is energy source saturated? (is current revenue == maximum source revenue)
        //          Evaluate source saturation
        //              Energy accessible per tick (EAP) = EnergyCapacity / ENERGY_REGEN_TIME;
        //              EAP per accessible tile  (EAPAT) = EAP / NumAccessibleTile
        //              For each accessible tile
        //                  Harvested Energy Per Tile (HEPT) = Sum of...
        //                  {
        //                      For each harvester assigned to this tile
        //                          Calculate its energy haversted per turn:
        //                              For mobile harvesters (work + carry + move):
        //                                  Total Carry Amount     = CARRY parts * CARRY_CAPACITY
        //                                  Harvesting Speed       = WORK  parts * HARVEST_POWER
        //                                  Turns harvesting       = Total Carry Amount / Harvesting Speed
        //                                  Turns moving           = (Number of turns from structure to Source) * 2
        //                                  Final Harvesting Speed = (Total Carry Amount) / (Turns harvesting + Turns moving)
        //                              For static harvesters (work + carry only):
        //                                  TODO
        //                  }

        _gbl.econMgr.AddRoom(m_room, _gbl);
    }
}
