#include "OwernshipLost.h"
#include "ai/dbg/Macros.h"
#include "ai/Global.h"

namespace ai::event::room
{
    void OwnershipLost::Process(ai::Global& _gbl) const
    {
        EVT_LOG(_gbl.dbg, "OwnershipLost: %s", m_roomName.c_str());

        _gbl.econMgr.RemoveRoom(m_roomName, _gbl);
    }
}
