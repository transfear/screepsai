#pragma once

#include "ai/event/IEvent.h"

namespace ai
{
    namespace object { class Room; }

    namespace event::room
    {
        class VisibilityAcquired : public ai::event::IEvent
        {
            public:
                inline VisibilityAcquired(object::Room& _room) : m_room(_room) { }
                
                virtual void Process(ai::Global& _gbl) const override;

            private:
                object::Room& m_room;
        };
    }
}
