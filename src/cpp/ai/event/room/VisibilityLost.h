#pragma once

#include <string>
#include "ai/event/IEvent.h"

namespace ai::event::room
{
    class VisibilityLost : public ai::event::IEvent
    {
        public:
            inline VisibilityLost(const std::string& roomName) : m_roomName(roomName) { }
            
            virtual void Process(ai::Global& _gbl) const override;

        private:
            std::string m_roomName;
    };
}
