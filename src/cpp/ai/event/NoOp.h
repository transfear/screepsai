#pragma once

#include "IEvent.h"

namespace ai::event
{
    class NoOp : public IEvent
    { 
        public:
            virtual void Process(ai::Global& _gbl) const override { }
    };
}
