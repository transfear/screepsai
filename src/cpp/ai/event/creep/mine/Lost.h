#pragma once

#include <string>
#include "ai/event/IEvent.h"

namespace ai::event::creep::mine
{
    class Lost : public ai::event::IEvent
    {
        public:
            inline Lost(const std::string& _creepId) : m_id(_creepId) { }
            
            virtual void Process(ai::Global& _gbl) const override;

        private:
            std::string m_id;
    };
}
