#include "Lost.h"
#include "ai/dbg/Macros.h"
#include "ai/Global.h"

namespace ai::event::creep::mine
{
    void Lost::Process(ai::Global& _gbl) const
    {
        EVT_LOG(_gbl.dbg, "creep::mine::Lost: %s", m_id.c_str());

        _gbl.myCreepMgr.OnCreepLost(m_id, _gbl);
    }
}
