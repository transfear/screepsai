#pragma once

#include "ai/event/IEvent.h"

namespace ai
{
    namespace object { class Creep; }

    namespace event::creep::mine
    {
        class Acquired : public ai::event::IEvent
        {
            public:
                inline Acquired(object::Creep& _creep) : m_creep(_creep) { }
                
                virtual void Process(ai::Global& _gbl) const override;

            private:
                object::Creep& m_creep;
        };
    }
}
