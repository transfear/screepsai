#include "Acquired.h"
#include "ai/dbg/Macros.h"
#include "ai/Global.h"

namespace ai::event::creep::mine
{
    void Acquired::Process(ai::Global& _gbl) const
    {
        EVT_LOG(_gbl.dbg, "creep::mine::Acquired: %s", m_creep.GetId().c_str());

        _gbl.myCreepMgr.OnCreepAcquired(m_creep, _gbl);        
    }
}
