#include "Appeared.h"
#include "ai/dbg/Macros.h"
#include "ai/Global.h"
#include "ai/object/Creep.h"

namespace ai::event::creep::enemy
{
    void Appeared::Process(ai::Global& _gbl) const
    {
        EVT_LOG(_gbl.dbg, "enemy::Appeared: %s", m_creep.GetId().c_str());
    }
}
