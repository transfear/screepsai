#include "Disappeared.h"
#include "ai/dbg/Macros.h"
#include "ai/Global.h"

namespace ai::event::creep::enemy
{
    void Disappeared::Process(ai::Global& _gbl) const
    {
        EVT_LOG(_gbl.dbg, "enemy::Disappeared: %s", m_id.c_str());
    }
}
