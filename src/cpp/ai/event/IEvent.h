#pragma once

namespace ai
{
    struct Global;

    namespace event
    {    
        class IEvent
        {
            public:
                inline virtual ~IEvent() { }
                virtual void Process(ai::Global& _gbl) const = 0;
        };
    }
}
