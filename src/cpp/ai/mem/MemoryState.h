#pragma once

#include <vector>

#include "ai/GameState.h"

#include "ai/serial/SerializableField.h"
#include "ai/serial/SerializableAggregate.h"
#include "ai/utils/TaggedTuple.h"

#include "object/Creep.h"
#include "object/Room.h"

#include <cstdio>

namespace ai::mem
{
    namespace MemoryStateSRL
    {
        struct Rooms  {};
        struct Creeps {};
        using Data = ai::utils::tagged_tuple::tagged_tuple
        <
            Rooms,  serial::SerializableField<std::vector<object::Room>>,
            Creeps, serial::SerializableField<std::vector<object::Creep>>
        >;
    }

    class MemoryState : public serial::SerializableAggregate<MemoryStateSRL::Data>
    {
        public: 
            using Room  = object::Room;
            using Creep = object::Creep;

            inline void ResetFrom(const ai::GameState& _gs);

            inline const std::vector<Room>&  GetRooms()  const { return m_data.get<MemoryStateSRL::Rooms>().get(); }
            inline       std::vector<Room>&  GetRooms()        { return m_data.get<MemoryStateSRL::Rooms>().get(); }
            inline const std::vector<Creep>& GetCreeps() const { return m_data.get<MemoryStateSRL::Creeps>().get(); }
            inline       std::vector<Creep>& GetCreeps()       { return m_data.get<MemoryStateSRL::Creeps>().get(); }
    };

    inline void MemoryState::ResetFrom(const ai::GameState& _gs)
    {
        // Rooms
        const ai::GameState::RoomMap& stateRooms = _gs.GetRooms();
        const uint32_t                numRooms   = stateRooms.size();

        std::vector<Room>& rooms = this->GetRooms();
        rooms.clear();
        rooms.reserve(numRooms);
        for (const auto& roomPair : stateRooms)
            rooms.emplace_back(roomPair.second);
        
        // Creeps
        const ai::GameState::CreepMap& stateCreeps = _gs.GetCreeps();
        const uint32_t                 numCreeps   = stateCreeps.size();

        std::vector<Creep>& creeps = this->GetCreeps();
        creeps.clear();
        creeps.reserve(numCreeps);
        for (const auto& creepPair : stateCreeps)
            creeps.emplace_back(creepPair.second);
        
        
        // TODO: add other unmanaged fields
    }
}
