#pragma once

#include <cstdint>

namespace ai
{
    struct Global;
    namespace mem
    {
        class MemoryState;

        class Memory
        {
            public:
                inline Memory(MemoryState& _memState) : m_PrevState(_memState) { }
                inline const MemoryState& GetPrevState() const { return m_PrevState; }

                void Load(ai::Global& _gbl);
                void Save(ai::Global& _gbl);

            private:

                static inline const uint16_t CURRENT_VERSION = 4;

                void DebugMemoryLoad(ai::Global& _gbl);
                MemoryState& m_PrevState;     // Memory state from the previous tick
        };
    }
}
