#include "Creep.h"

#include "ai/object/Creep.h"

namespace ai::mem::object
{
    Creep::Creep(const ai::object::Creep& _obj)
    : SerializableAggregate(_obj.GetId(), static_cast<uint8_t>(_obj.GetFlags()))
    {
    }
}
