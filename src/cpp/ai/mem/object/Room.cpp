#include "Room.h"

#include "ai/object/Room.h"

#include <cstdio>

namespace ai::mem::object
{
    Room::Room(const ai::object::Room& _obj)
    : SerializableAggregate(_obj.GetName(), static_cast<uint8_t>(_obj.GetFlags()))
    {
    }
}
