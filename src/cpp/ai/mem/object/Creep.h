#pragma once

#include "ai/serial/SerializableAggregate.h"
#include "ai/serial/SerializableField.h"
#include "ai/utils/TaggedTuple.h"

#include "ai/object/Creep.h"

namespace ai
{
    namespace object { class Creep; }

    namespace mem::object
    {
        namespace CreepSRL
        {
            struct Id {};
            struct Flags {};
            using Data = ai::utils::tagged_tuple::tagged_tuple
            <
                Id,    serial::SerializableField<std::string>,
                Flags, serial::SerializableField<uint8_t>
            >;
        }

        class Creep : public serial::SerializableAggregate<CreepSRL::Data>
        {
            public:
                inline Creep() { }
                Creep(const ai::object::Creep& _obj);

                inline const std::string& GetId() const { return m_data.get<CreepSRL::Id>().get(); }
                inline       uint8_t   GetFlags() const { return m_data.get<CreepSRL::Flags>().get(); }
        };
    }
}
