#pragma once

#include "ai/serial/SerializableAggregate.h"
#include "ai/serial/SerializableField.h"
#include "ai/utils/TaggedTuple.h"

namespace ai
{
    namespace object { class Room; }

    namespace mem::object
    {
        namespace RoomSRL
        {
            struct Name  {};
            struct Flags {};
            using Data = ai::utils::tagged_tuple::tagged_tuple
            <
                Name,  serial::SerializableField<std::string>,
                Flags, serial::SerializableField<uint8_t>
            >;
        }

        class Room : public serial::SerializableAggregate<RoomSRL::Data>
        {
            public:
                inline Room() { }
                Room(const ai::object::Room& _obj);

                inline const std::string& GetName() const { return m_data.get<RoomSRL::Name>().get(); }
                inline       uint8_t     GetFlags() const { return m_data.get<RoomSRL::Flags>().get(); }
        };
    }
}
