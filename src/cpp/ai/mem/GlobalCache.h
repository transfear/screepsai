#pragma once

#include <memory>

#include "ai/creep/mine/Manager.h"
#include "ai/econ/Manager.h"
#include "ai/mem/MemoryState.h"

namespace ai
{
    namespace creep::mine { class Manager; }
    namespace econ { class Manager; }

    namespace mem
    {
        class MemoryState;

        // Things here are stored in the heap, which stays valid across a random amount of ticks.
        // Important stuff should be stored in Memory.
        struct GlobalCache
        {
            static bool Init();

            bool isValid = false;

            // Store these here to prevent having to deserialize from memory every frame
            std::unique_ptr<MemoryState>          pMemoryState = nullptr;
            std::unique_ptr<creep::mine::Manager> pCreepMgr    = nullptr;
            std::unique_ptr<econ::Manager>        pEconMgr     = nullptr;
        };

        extern GlobalCache g_cache;
    }
}
