#include "Memory.h"
#include "MemoryState.h"

#include "cppreeps/cppreeps.h"
#include "cppreeps/lzw.h"

#include "ai/dbg/Macros.h"
#include "ai/serial/stream/BinaryStream.h"
#include "ai/Global.h"

namespace ai::mem
{
    void Memory::Load(ai::Global& _gbl)
    {
        js::val& mem    = screeps::tick->Memory;
        js::val  js_enc = mem["raw"];

        std::wstring encodedString;
        if (!js_enc.isNull() && !js_enc.isUndefined())
            encodedString = js_enc.as<std::wstring>();

        if (encodedString.length() > 0)
        {
            std::vector<unsigned char> decoded;
            screeps::lzw_decode_binary(encodedString, decoded);

            ai::serial::stream::BinaryReadStream stream(decoded);

            uint16_t version;
            stream.AlignTo(std::alignment_of<decltype(version)>::value);
            stream.Read(&version, sizeof(version));

            if (version == Memory::CURRENT_VERSION)
            {
                m_PrevState.DeserializeFrom(stream);
                _gbl.myCreepMgr.DeserializeFrom(stream);
                _gbl.econMgr.DeserializeFrom(stream);
            }
            else
            {
                DESRL_LOG(_gbl.dbg, "Wrong Memory version. Expected %u, got %u", Memory::CURRENT_VERSION, version);
            }
        }

        DebugMemoryLoad(_gbl);
    }
    
    void Memory::Save(ai::Global& _gbl)
    {
        m_PrevState.ResetFrom(_gbl.gs);

        ai::serial::stream::BinaryWriteStream stream;

        const uint16_t version = Memory::CURRENT_VERSION;
        stream.AlignTo(std::alignment_of<decltype(version)>::value);
        stream.Write(&version, sizeof(version));

        m_PrevState.SerializeTo(stream);
        _gbl.myCreepMgr.SerializeTo(stream);
        _gbl.econMgr.SerializeTo(stream);

        // Encode in raw memory
        std::wstring encoded = screeps::lzw_encode_binary(stream.get());
        javascript::val& mem = screeps::tick->Memory;
        mem.set("raw", encoded);
    }

    void Memory::DebugMemoryLoad(ai::Global& _gbl)
    {
        DESRL_LOG(_gbl.dbg, "MEMORY LOAD (v%u)", Memory::CURRENT_VERSION);

        // Memorized rooms
        using Room = MemoryState::Room;
        const std::vector<Room>& rooms = m_PrevState.GetRooms();
        const uint32_t roomCount = rooms.size();
        DESRL_LOG(_gbl.dbg, "    %u rooms:", roomCount);
        for (uint32_t i = 0; i < roomCount; ++i)
        {
            const Room& room = rooms[i];
            DESRL_LOG(_gbl.dbg, "        %u: %s", i, room.GetName().c_str());
            DESRL_LOG(_gbl.dbg, "            Flags: 0x%02x", room.GetFlags());
        }

        // Memorized creeps
        using Creep = MemoryState::Creep;
        const std::vector<Creep>& creeps = m_PrevState.GetCreeps();
        const uint32_t creepCount = creeps.size();
        DESRL_LOG(_gbl.dbg, "    %u creeps:", creepCount);
        for (uint32_t i = 0; i < creepCount; ++i)
        {
            const Creep& creep = creeps[i];
            DESRL_LOG(_gbl.dbg, "        %u: %s", i, creep.GetId().c_str());
            DESRL_LOG(_gbl.dbg, "            Flags: 0x%02x", creep.GetFlags());
        }
    }
}
