#include "GlobalCache.h"

namespace ai::mem
{
    GlobalCache g_cache = GlobalCache();

    bool GlobalCache::Init()
    {
        if (g_cache.isValid)
            return false;
        
        g_cache.pMemoryState = std::make_unique<MemoryState>();
        g_cache.pCreepMgr    = std::make_unique<creep::mine::Manager>();
        g_cache.pEconMgr     = std::make_unique<econ::Manager>();

        g_cache.isValid = true;
        return true;
    }
}
