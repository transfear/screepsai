#pragma once

#include <unordered_map>

#include "cppreeps/cppreeps.h"
#include "object/Creep.h"
#include "object/Room.h"

namespace ai
{
    class GameState
    {
        public:
            using RoomMap     = std::unordered_map<std::string, ai::object::Room>;
            using CreepMap    = std::unordered_map<std::string, ai::object::Creep>;

            GameState() { Initialize(); }
            ~GameState() { }
            
            inline const RoomMap& GetRooms()   const { return m_rooms; }
            inline const CreepMap& GetCreeps() const { return m_creeps; }

        private:
            void Initialize();
            void InitializeAllMyCreeps(const js::val& _jsGame);
            void InitializeAllMyConstructionSites(const js::val& _jsGame);
            void InitializeAllMyStructures(const js::val& _jsGame);
            void InitializeVisibleRooms(const js::val& _jsGame);
            void InitializeHostileCreepsInRoom(const js::val& _jsRoom, object::Room& _roomObj);

            RoomMap  m_rooms;
            CreepMap m_creeps;
    };
}
