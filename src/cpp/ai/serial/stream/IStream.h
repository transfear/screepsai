#pragma once

#include <vector>

namespace ai::serial::stream
{
    class IStream
    {
        public:
            inline virtual ~IStream() { }

            virtual void Read(void* _dst, size_t _sizeOf)        = 0;
            virtual void Write(const void* _src, size_t _sizeOf) = 0;

            virtual void Skip(size_t _byteCount)      = 0;
            virtual const void* GetReadCursor() const = 0;
            virtual size_t GetCursor() const = 0;

            virtual void AlignTo(size_t _alignment) = 0;
    };
}
