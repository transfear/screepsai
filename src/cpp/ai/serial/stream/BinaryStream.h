#pragma once

#include <vector>
#include "IStream.h"

namespace ai::serial::stream
{
    class BinaryReadStream : public IStream
    {
        public:
            inline BinaryReadStream(const std::vector<unsigned char>& _stream) : m_stream(_stream) { }

            inline virtual void Read(void* _dst, size_t _sizeOf) override
            {
                memcpy(_dst, GetReadCursor(), _sizeOf);
                m_readCursor += _sizeOf;
            }

            inline virtual void Write(const void* _src, size_t _sizeOf) override
            {
                // todo: fatal assert
            }

            inline virtual void        Skip(size_t _byteCount) override { m_readCursor += _byteCount; }
            inline virtual const void* GetReadCursor() const   override { return &m_stream[m_readCursor]; }

            inline virtual void AlignTo(size_t _alignment) override
            {
                const uint32_t offAlignment = m_readCursor & (_alignment - 1);
                if (offAlignment != 0)
                    m_readCursor += _alignment - offAlignment;
            }

            inline virtual size_t GetCursor() const override { return m_readCursor; }

        private:
            const std::vector<unsigned char>& m_stream;
            size_t m_readCursor = 0;
    };

    class BinaryWriteStream : public IStream
    {
        public:
            inline virtual void Read(void* _dst, size_t _sizeOf) override
            {
                // todo: fatal assert
            }
            inline virtual void Skip(size_t _byteCount) override
            {
                // todo: fatal assert
            }
            inline virtual const void* GetReadCursor() const override
            {
                // todo: fatal assert
                return nullptr;
            }

            inline virtual void Write(const void* _src, size_t _sizeOf) override
            {
                const unsigned char* pIter = reinterpret_cast<const unsigned char*>(_src);
                const unsigned char* pEnd  = pIter + _sizeOf;
                m_stream.insert(m_stream.end(), pIter, pEnd);
            }           

            inline virtual void AlignTo(size_t _alignment) override
            {
                // TODO: Make a single insertion of the appropriate size
                while ((m_stream.size() & (_alignment - 1)) != 0)
                    m_stream.emplace_back(0xCA);
            }

            inline virtual size_t GetCursor() const override { return m_stream.size(); }
            inline const std::vector<unsigned char>& get() const { return m_stream; }

        private:
            std::vector<unsigned char> m_stream;
    };
}
