#pragma once
#include "stream/IStream.h"

namespace ai
{
    struct Global;
    
    namespace serial
    {
        class ISerializable
        {
            public:
                virtual ~ISerializable() { }

                // For regular fields
                virtual void DeserializeFrom(stream::IStream& stream) = 0;
                virtual void SerializeTo(stream::IStream& stream) const = 0;

                // For fields that are pointers
                virtual void FixupPointer(ai::Global& _gbl) = 0;
        };
    }
}
