#pragma once

#include <cstdio>
#include <tuple>
#include "SerializableField.h"

namespace ai
{
    struct Global;

    namespace serial
    {
        template <typename RuntimeType, typename SerializedType, typename Converter>
        class SerializablePointer : public SerializableField<SerializedType>
        {
            public:
                inline SerializablePointer() { }
                inline SerializablePointer(RuntimeType _r, const SerializedType& _e) : SerializableField<SerializedType>(_e), m_ptr(_r) { }
                template <typename RT, typename ST>
                inline SerializablePointer(const std::tuple<RT,ST>&& _t);
                

                // From ISerializablePtr
                inline virtual void FixupPointer(ai::Global& _gbl) override;

                inline const RuntimeType& getPtr() const { return m_ptr; }

            protected:

                RuntimeType m_ptr = nullptr;
        };

        template <typename RuntimeType, typename SerializedType, typename Converter>
        template <typename RT, typename ST>
        inline SerializablePointer<RuntimeType, SerializedType, Converter>::SerializablePointer(const std::tuple<RT,ST>&& _t)
        : SerializablePointer
        (
            std::forward<RT>(std::get<0>(std::forward<const std::tuple<RT,ST>>(_t))), 
            std::forward<ST>(std::get<1>(std::forward<const std::tuple<RT,ST>>(_t)))
        )
        {
        }

        template <typename RuntimeType, typename SerializedType, typename Converter>
        inline void SerializablePointer<RuntimeType, SerializedType, Converter>::FixupPointer(ai::Global& _gbl)
        {
            Converter c;
            m_ptr = c.GetPtr(_gbl, this->get());
        }
    }
}
