#pragma once

#include <cstdio>
#include <vector>
#include "Interface.h"

namespace ai::serial
{
    template <typename T>
    class SerializableAggregate : public ISerializable
    {
        public:
            // Forward arguments to tagged_tuple
            template<typename... Args>
            SerializableAggregate(Args &&...args) : m_data(std::forward<Args>(args)...) { }

            // From ISerializable
            inline virtual void DeserializeFrom(stream::IStream& _stream) override;
            inline virtual void SerializeTo(stream::IStream& _stream) const override;
            inline virtual void FixupPointer(ai::Global& _gbl) override;

        protected:
            T m_data;
    };

    template <typename T>
    inline void SerializableAggregate<T>::SerializeTo(stream::IStream& _stream) const
    {
        m_data.static_for_const([&] (const auto& field) { field.SerializeTo(_stream); });
    }

    template <typename T>
    inline void SerializableAggregate<T>::DeserializeFrom(stream::IStream& _stream)
    {
        m_data.static_for([&] (auto& field) { field.DeserializeFrom(_stream); });
    }

    template <typename T>
    inline void SerializableAggregate<T>::FixupPointer(ai::Global& _gbl)
    {
        m_data.static_for([&] (auto& field) { field.FixupPointer(_gbl); });
    } 
}
