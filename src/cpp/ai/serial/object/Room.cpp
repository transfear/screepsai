#include "Room.h"
#include "ai/Global.h"

#include <cstdio>

namespace ai::serial::object
{
    const ai::object::Room* RoomConverter::GetPtr(ai::Global& _gbl, const std::string& _name) const
    {
        const GameState::RoomMap& rooms = _gbl.gs.GetRooms();
        auto it = rooms.find(_name);
        if (it == rooms.end())
            return nullptr;
        
        return &it->second;
    }
}
