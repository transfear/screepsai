#include "Creep.h"
#include "ai/Global.h"

namespace ai::serial::object
{
    const ai::object::Creep* CreepConverter::GetPtr(ai::Global& _gbl, const std::string& _id) const
    {
        const GameState::CreepMap& creeps = _gbl.gs.GetCreeps();
        auto it = creeps.find(_id);
        if (it == creeps.end())
            return nullptr;
        
        return &it->second;
    }
}
