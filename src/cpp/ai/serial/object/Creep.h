#pragma once

#include <string>
#include "ai/serial/SerializablePointer.h"

namespace ai
{   
    struct Global;
    namespace object { class Creep; }

    namespace serial::object
    {
        class CreepConverter
        {
            public:
                const ai::object::Creep* GetPtr(ai::Global& _gbl, const std::string& _id) const;
        };

        using Creep = ai::serial::SerializablePointer<const ai::object::Creep*, std::string, CreepConverter>;
    }
}
