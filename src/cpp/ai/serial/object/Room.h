#pragma once

#include <string>
#include "ai/serial/SerializablePointer.h"

namespace ai
{   
    struct Global;
    namespace object { class Room; }

    namespace serial::object
    {
        class RoomConverter
        {
            public:
                const ai::object::Room* GetPtr(ai::Global& _gbl, const std::string& _name) const;
        };

        using Room = ai::serial::SerializablePointer<const ai::object::Room*, std::string, RoomConverter>;
    }
}
