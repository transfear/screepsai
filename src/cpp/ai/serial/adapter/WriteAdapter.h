#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <type_traits>

#include "ai/serial/Interface.h"
#include "ai/serial/stream/IStream.h"

namespace ai::serial::adapter
{
    class WriteAdapter
    {
        public:
            template <typename CharType>
            inline void Write(const std::basic_string<CharType>& elem, stream::IStream& _stream) const
            {
                const size_t strLen = elem.length();
                this->Write(strLen,_stream);


                const CharType* pBuf = elem.data();
                _stream.AlignTo(std::alignment_of<CharType>::value);
                _stream.Write(pBuf, strLen);
            }

            inline void Write(const ISerializable& elem, stream::IStream& _stream) const
            {
                elem.SerializeTo(_stream);
            }

            template <typename FieldType>
            inline void Write(const std::vector<FieldType>& elem, stream::IStream& _stream) const
            {
                const size_t size = elem.size();
                this->Write(size, _stream);

                for (const FieldType& i : elem)
                    this->Write(i, _stream);
            }

            template <typename KeyType, typename ValueType>
            inline void Write(const std::unordered_map<KeyType,ValueType>& elem, stream::IStream& _stream) const
            {
                const size_t size = elem.size();
                this->Write(size, _stream);

                for (const auto& keyValuePair : elem)
                {
                    this->Write(keyValuePair.first, _stream);
                    this->Write(keyValuePair.second, _stream);
                }
            }

            template <typename FieldType>
            inline typename std::enable_if<!std::is_base_of<ISerializable, FieldType>::value>::type Write(const FieldType& elem, stream::IStream& _stream) const
            {
                static_assert(std::is_standard_layout<FieldType>::value);
                static_assert(!std::is_pointer<FieldType>::value);

                _stream.AlignTo(std::alignment_of<FieldType>::value);
                _stream.Write(&elem, sizeof(elem));
            }
    };
}
