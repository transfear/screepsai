#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <type_traits>

#include "ai/serial/Interface.h"

namespace ai
{
    struct Global;

    namespace serial::adapter
    {
        class FixupPtrAdapter
        {
            public:
                template <typename FieldType>
                inline void FixupPointer(std::vector<FieldType>& elem, ai::Global& _gbl) const
                {
                    for (FieldType& i : elem)
                        this->FixupPointer(i, _gbl);
                }

                template <typename KeyType, typename ValueType>
                inline void FixupPointer(std::unordered_map<KeyType,ValueType>& elem, ai::Global& _gbl) const
                {
                    for (auto& keyValuePair : elem)
                    {
                        this->FixupPointer(keyValuePair.first, _gbl);
                        this->FixupPointer(keyValuePair.second, _gbl);
                    }
                }

                inline void FixupPointer(ISerializable& elem, ai::Global& _gbl) const
                {
                    elem.FixupPointer(_gbl);
                }
                                
                template <typename FieldType>
                inline typename std::enable_if<!std::is_base_of<ISerializable, FieldType>::value>::type FixupPointer(const FieldType&, ai::Global&) const
                {
                    // This function is no-op, so it's only ok for standard layout types
                    // Not containers. Not pointers.
                    static_assert(std::is_standard_layout<FieldType>::value);
                    static_assert(!std::is_pointer<FieldType>::value);
                }
        };
    }
}
