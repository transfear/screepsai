#pragma once

#include <cstdio>
#include <string>
#include <unordered_map>
#include <vector>
#include <type_traits>

#include "ai/serial/Interface.h"
#include "ai/serial/stream/IStream.h"

namespace ai::serial::adapter
{
    class ReadAdapter
    {
        public:
            template <typename CharType>
            inline void Read(std::basic_string<CharType>& elem, stream::IStream& _stream) const
            {
                size_t strLen;
                this->Read(strLen, _stream);

                _stream.AlignTo(std::alignment_of<CharType>::value);

                const CharType* pStrBuf = reinterpret_cast<const CharType*>(_stream.GetReadCursor());
                elem = std::basic_string<CharType>(pStrBuf, strLen);

                _stream.Skip(strLen);
            }

            inline void Read(ISerializable& elem, stream::IStream& _stream) const
            {
                elem.DeserializeFrom(_stream);
            }

            template <typename FieldType>
            inline void Read(std::vector<FieldType>& elem, stream::IStream& _stream) const
            {
                size_t size;
                this->Read(size, _stream);

                elem.reserve(size);               
                for (unsigned int i = 0; i < size; ++i)
                {
                    FieldType& item = elem.emplace_back();
                    this->Read(item, _stream);
                }
            }

            template <typename KeyType, typename ValueType>
            inline void Read(std::unordered_map<KeyType,ValueType>& elem, stream::IStream& _stream) const
            {
                size_t size;
                this->Read(size, _stream);

                for (unsigned int i = 0; i < size; ++i)
                {
                    KeyType key;
                    this->Read(key, _stream);

                    ValueType value;
                    this->Read(value, _stream);

                    elem.insert({ key, value });
                }
            }

            template <typename FieldType>
            inline typename std::enable_if<!std::is_base_of<ISerializable, FieldType>::value>::type Read(FieldType& elem, stream::IStream& _stream) const
            {
                static_assert(std::is_standard_layout<FieldType>::value);
                static_assert(!std::is_pointer<FieldType>::value);
                
                _stream.AlignTo(std::alignment_of<FieldType>::value);
                _stream.Read(&elem, sizeof(elem));
            }
    };
}
