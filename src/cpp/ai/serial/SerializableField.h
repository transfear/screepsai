#pragma once

#include <tuple>
#include <type_traits>

#include "Interface.h"
#include "adapter/ReadAdapter.h"
#include "adapter/WriteAdapter.h"
#include "adapter/FixupPtrAdapter.h"

namespace ai::serial
{
    template <typename FieldType>
    class SerializableField : public ISerializable
    {
        static_assert(!std::is_reference<FieldType>::value);
        static_assert(!std::is_pointer<FieldType>::value);

        public:
            inline SerializableField() { }
            inline SerializableField(const FieldType& _e) : elem(_e) { }

            inline const FieldType& get() const      { return elem; }
            inline       FieldType& get()            { return elem; }
            inline void             set(const FieldType& _e) { elem = _e; }

            inline operator FieldType() { return elem; }
            inline SerializableField<FieldType>& operator=(const FieldType& _e);

            // From ISerializable
            inline virtual void DeserializeFrom(stream::IStream& stream) override;
            inline virtual void SerializeTo(stream::IStream& stream) const override;

            inline virtual void FixupPointer(ai::Global& _gbl) override;
        
        protected:
            FieldType elem;
    };

    template <typename FieldType>
    inline SerializableField<FieldType>& SerializableField<FieldType>::operator=(const FieldType& _e)
    {
        this->elem = _e;
        return *this;
    }

    template <typename FieldType>
    inline void SerializableField<FieldType>::DeserializeFrom(stream::IStream& stream)
    {        
        adapter::ReadAdapter adapter;
        adapter.Read(this->elem, stream);
    }

    template <typename FieldType>
    inline void SerializableField<FieldType>::SerializeTo(stream::IStream& stream) const
    {
        adapter::WriteAdapter adapter;
        adapter.Write(this->elem, stream);
    }

    template <typename FieldType>
    inline void SerializableField<FieldType>::FixupPointer(ai::Global& _gbl)
    {
        adapter::FixupPtrAdapter adapter;
        adapter.FixupPointer(this->elem, _gbl);
    }
}
