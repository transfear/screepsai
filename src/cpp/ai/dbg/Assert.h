#pragma once

#include "Macros.h"

#if NO_ASSERT

    #define AI_ASSERT(dbg, x) do { } while (false)
    #define AI_ASSERTEX(dbg, x, text, ...) do { } while (false)

#else

    #define AI_ASSERT(dbg, x) \
    {\
        if (!(x))\
            FATAL_LOG(dbg, __FILE__ "(%u):" #x, __LINE__);\
    }

    #define AI_ASSERTEX(dbg, x, text, ...)\
    {\
        if (!(x))\
            FATAL_LOG(dbg, __FILE__ "(%u):" #x ", " text, __LINE__, __VA_ARGS__);\
    }

#endif
