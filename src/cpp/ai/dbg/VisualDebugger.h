#pragma once

#include <cstdarg>
#include <string>

#include "Flags.h"

namespace ai
{
    struct Global;

    namespace dbg
    {
        class VisualDebugger
        {
            public:
                VisualDebugger();
                void PrintText(uint32_t _uiRGB, const char* _fmt, ...);

                template <Flags neededFlag>
                inline void PrintTextIf(uint32_t _uiRGB, const char* _fmt, ...);

            private:

                void PrintText(uint32_t _uiRGB, const char* _fmt, va_list& _args);

                uint32_t m_curTextLine       = 0;
                bool     m_showTextInConsole = false;
                Flags    m_activeFlags       = Flags::All; 
        };

        template <Flags neededFlag>
        inline void VisualDebugger::PrintTextIf(uint32_t _uiRGB, const char* _fmt, ...)
        {
            if ((neededFlag & m_activeFlags) != Flags::None)
            {
                va_list args;
                va_start(args, _fmt);
                PrintText(_uiRGB, _fmt, args);
                va_end(args);
            }
        }
    }
}
