#pragma once

namespace ai::dbg
{
    enum Flags
    {
        None            = 0x00,
        Fatal           = 0x01,
        Serialization   = 0x02,
        Deserialization = 0x04,
        Events          = 0x08,
        Profiler        = 0x0f,

        All             = 0xFF
    };
}
