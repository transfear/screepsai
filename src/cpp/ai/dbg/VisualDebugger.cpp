#include "VisualDebugger.h"

#include <stdlib.h>
#include "cppreeps/cppreeps.h"
#include "ai/dbg/Assert.h"

namespace ai::dbg
{
    VisualDebugger::VisualDebugger()
    {
        javascript::val& mem    = screeps::tick->Memory;
        javascript::val  js_dbg = mem["dbg"];

        if (!js_dbg.isNull() && !js_dbg.isUndefined())
        {
            m_activeFlags = static_cast<Flags>(js_dbg.as<int>());
        }
        else
        {
            mem.set("dbg", static_cast<int>(m_activeFlags));
        }
    }

    void VisualDebugger::PrintText(uint32_t _uiRGB, const char* _fmt, ...)
    {
        va_list args;
        va_start(args, _fmt);
        PrintText(_uiRGB, _fmt, args);
        va_end(args);
    }

    void VisualDebugger::PrintText(uint32_t _uiRGB, const char* _fmt, va_list& _args)
    {
        char text[1024];
        vsnprintf(text, sizeof(text), _fmt, _args);

        if (m_showTextInConsole)
        {
            std::puts(text);
        }
        else
        {
            // convert to web color
            const uint8_t red   = (_uiRGB >> 16) & 0xff;
            const uint8_t green = (_uiRGB >>  8) & 0xff;
            const uint8_t blue  = (_uiRGB >>  0) & 0xff;
            char color[7] = { '\0' };
            snprintf(&color[0], 3, "%02x", red);
            snprintf(&color[2], 3, "%02x", green);
            snprintf(&color[4], 3, "%02x", blue);   

            // create a global room visual
            char buffer[1024];
            snprintf(buffer, sizeof(buffer), 
                "new RoomVisual().text('%s', 0, %u, { color: '#%s', align: 'left', font: '0.6 monospace'});",
                text, m_curTextLine, color
            );
            emscripten_run_script(buffer);
            ++m_curTextLine;   
        }
    }
}
