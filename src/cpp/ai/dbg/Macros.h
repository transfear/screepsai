#pragma once

#include <stdint.h>
#include "Flags.h"

namespace ai::dbg
{
    constexpr uint32_t RED   = 0xff0000;
    constexpr uint32_t GREEN = 0x00ff00;
    constexpr uint32_t WHITE = 0xffffff;
    constexpr uint32_t GREY  = 0x808080;
}

#if 1
    #define FATAL_LOG(vdbg, ...) (vdbg).PrintTextIf<ai::dbg::Flags::Fatal>(ai::dbg::RED, __VA_ARGS__)
#else
    #define FATAL_LOG(vdbg, ...)
#endif

#if 1
    #define DESRL_LOG(vdbg, ...) (vdbg).PrintTextIf<ai::dbg::Flags::Deserialization>(ai::dbg::GREY, __VA_ARGS__)
#else
    #define DESRL_LOG(vdbg, ...)
#endif

#if 1
    #define EVT_LOG(vdbg, ...) (vdbg).PrintTextIf<ai::dbg::Flags::Events>(ai::dbg::WHITE, __VA_ARGS__)
#else
    #define EVT_LOG(vdbg, ...)
#endif

#if 1
    #define PROF_LOG(vdbg, ...) (vdbg).PrintTextIf<ai::dbg::Flags::Profiler>(0x3380FF, __VA_ARGS__)
#else
    #define PROF_LOG(vdbg, ...)
#endif
