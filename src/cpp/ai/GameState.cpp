#include "GameState.h"

#include "ai/utils/Alliance.h"
#include "ai/utils/Constants.h"


namespace ai
{
    void GameState::Initialize()
    {
        const js::val& js_game = screeps::tick->Game;

        InitializeVisibleRooms(js_game);
        InitializeAllMyStructures(js_game);
        InitializeAllMyCreeps(js_game);
        InitializeAllMyConstructionSites(js_game);

        // TODO: loop on other stuff
    }

    void GameState::InitializeAllMyCreeps(const js::val& _jsGame)
    {
        js::object_native myCreepsObj;
        js::js_object_to_map(_jsGame["creeps"], myCreepsObj);
        for (const std::pair<std::string, js::val>& creepPair : myCreepsObj)
        {
            const js::val&      js_creep = creepPair.second;
            const std::string   creepId  = js_creep["id"].as<std::string>();
            const js::val&      js_room  = js_creep["room"];
            object::Room&       roomObj  = m_rooms.find(js_room["name"].as<std::string>())->second;

            m_creeps.emplace_hint(m_creeps.end(), std::piecewise_construct, std::make_tuple(creepId), std::forward_as_tuple(js_creep, creepId, object::CreepFlags::kMine, roomObj));
        }
    }

    void GameState::InitializeAllMyStructures(const js::val& _jsGame)
    {
        js::object_native myStructuresObj;
        js::js_object_to_map(_jsGame["structures"], myStructuresObj);
        for (const std::pair<std::string, js::val>& structurePair : myStructuresObj)
        {
            //const std::string& structureId  = structurePair.first;
            const js::val&     js_structure = structurePair.second;
            const js::val&     js_room      = js_structure["room"];
            object::Room&      roomObj      = m_rooms.find(js_room["name"].as<std::string>())->second;
            roomObj.AddStructure(js_structure);
        }
    }

    void GameState::InitializeAllMyConstructionSites(const js::val& _jsGame)
    {
        js::object_native myCSitesObj;
        js::js_object_to_map(_jsGame["constructionSites"], myCSitesObj);
        for (const std::pair<std::string, js::val>& csitePair : myCSitesObj)
        {
            //const std::string& csiteId  = csitePair.first;
            const js::val&     js_csite = csitePair.second;
            const js::val&     js_room  = js_csite["room"];
            object::Room&      roomObj  = m_rooms.find(js_room["name"].as<std::string>())->second;
            roomObj.AddConstructionSite(js_csite);
        }
    }

    void GameState::InitializeVisibleRooms(const js::val& _jsGame)
    {
        // Find all visible rooms, and save them
        js::object_native visibleRooms;
        js::js_object_to_map(_jsGame["rooms"], visibleRooms);
        for (const std::pair<std::string, js::val>& roomPair : visibleRooms)
        {
            // Check if room is owned by me
            const std::string& roomName = roomPair.first;
            const js::val&   js_roomObj = roomPair.second;
            
            const js::val& roomController = js_roomObj["controller"];
            const bool isMine = roomController["my"].as<bool>();

            const object::RoomFlags roomFlags = object::RoomFlags::kVisible | (isMine ? object::RoomFlags::kOwned : object::RoomFlags::kNone);
            auto it = m_rooms.emplace_hint(m_rooms.end(), std::piecewise_construct, std::make_tuple(roomName), std::forward_as_tuple(js_roomObj, roomName, roomFlags));
            
            object::Room& room = it->second;

            // Initialize all hostile creeps in this room
            InitializeHostileCreepsInRoom(js_roomObj, room);

            // TODO: Initialize hostile structs
            // TODO: Initialize hostile construction sites
        }
    }

    void GameState::InitializeHostileCreepsInRoom(const js::val& _jsRoom, object::Room& _roomObj)
    {
        // TODO: do this only in rooms on my territory boundaries + remote rooms I'm exploring

        // Find all hostile creeps in the room
        js::val js_creeps_obj = _jsRoom.call<js::val>("find", utils::constants::FIND_HOSTILE_CREEPS);
        js::array_native js_creeps;
        js::js_array_to_vector(js_creeps_obj, js_creeps);

        for (const js::val& js_creep : js_creeps)
        {
            const std::string creepId = js_creep["id"].as<std::string>();

            object::CreepFlags creepFlags = object::CreepFlags::kNone;
            const std::string  playerName = js_creep["owner"]["username"].as<std::string>();

            if (utils::alliance::IsAlliedPlayer(playerName))
                creepFlags = object::CreepFlags::kAllied;
            else
                creepFlags = object::CreepFlags::kEnemy;

            m_creeps.emplace_hint(m_creeps.end(), std::piecewise_construct, std::make_tuple(creepId), std::forward_as_tuple(js_creep, creepId, creepFlags, _roomObj));
        }
    }
}
