#pragma once

#include "dbg/VisualDebugger.h"
#include "GameState.h"
#include "mem/Memory.h"
#include "creep/mine/Manager.h"
#include "event/Manager.h"
#include "econ/Manager.h"

namespace ai
{
    struct Global
    {
        Global();
        inline void FixupPointers();

        GameState       gs;
        mem::Memory     mem;

        creep::mine::Manager& myCreepMgr;
        econ::Manager&        econMgr;
        event::Manager        eventMgr;

        dbg::VisualDebugger  dbg;
    };

    inline void Global::FixupPointers()
    {
        myCreepMgr.FixupPointer(*this);
        econMgr.FixupPointer(*this);
    }
}