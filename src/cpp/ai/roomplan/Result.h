#pragma once

#include <vector>

#include "ai/object/RoomPosition.h"
#include "ai/utils/Constants.h"

namespace ai
{
    namespace object { class Room; }

    namespace roomplan
    {
        class Result
        {
            public:
                using Entry      = std::pair<ai::utils::constants::STRUCTURE, ai::object::RoomPosition>;
                using EntryArray = std::vector<Entry>;

                inline const EntryArray& GetBuildings() const { return m_buildings; }
                inline       EntryArray& GetBuildings()       { return m_buildings; }

                inline const EntryArray& GetRoads() const { return m_roads; }
                inline       EntryArray& GetRoads()       { return m_roads; }

                void Draw(const ai::object::Room& _room) const;

            private:
                EntryArray m_buildings;
                EntryArray m_roads;
        };
    }
}