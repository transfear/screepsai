#include "Planner.h"

#include <algorithm>
#include <numeric>
#include <queue>
#include <vector>

#include "BuildingList.h"
#include "Result.h"
#include "SortedPosPlan.h"
#include "treefill/Generator.h"

#include "ai/dbg/Assert.h"
#include "ai/dbg/Macros.h"
#include "ai/Global.h"
#include "ai/object/Room.h"
#include "ai/object/StructureRampart.h"
#include "ai/utils/Constants.h"

/*
Links:
- Up to 6, should keep 1 per energy source or mineral patch

From center to outwards:
- Observer
- Nuker
- Power Spawn
- Terminal
- Storage
- Lab
- Spawn
- Extension

Build priority list above, growing outward from center.
- Must keep every building accessible.
- Must keep all room exits accessibles.
- Must keep all sources/mineral patches/harvesting spots accessible.

Insert remaining links at base edges

Insert (up to 6) towers at geometrical means?
- No need to be closer than ~(TOWER_OPTIMAL_RANGE - 3?)to structure edges
*/

namespace ai::roomplan
{
    void Planner::PlanAllRooms(ai::Global& _gbl)
    {
        // TODO: Move this to Constants.h in a static_assert when C++20 is properly supported
        AI_ASSERT(_gbl.dbg, std::is_sorted(ai::utils::constants::STRUCTURE_OBSTACLES.begin(), ai::utils::constants::STRUCTURE_OBSTACLES.end()));

        const ai::GameState::RoomMap& roomMap = _gbl.gs.GetRooms();
        for (const auto& room : roomMap)
            this->Plan(_gbl, room.second);
    }

    void Planner::Plan(ai::Global& _gbl, const ai::object::Room& _roomObj)
    {
        javascript::val& game = screeps::tick->Game;
        javascript::val  cpu  = game["cpu"];
        const float startMS   = cpu.call<float>("getUsed");

        // Calc how much it costs to move on the terrain
        ai::object::Terrain mvmtCost;
        GenerateMovementCost(_roomObj, mvmtCost);

        // Find best location to place our buildings
        ai::object::DistanceTerrain prefMap(0);
        BuildPreferenceMap(_gbl, _roomObj, mvmtCost, prefMap);

        // Find best order in which we should place our buildings
        SortedPosPlan posPlan(_roomObj);
        posPlan.FromPrefMap(prefMap);
        //posPlan.Draw();

        // Find which buildings we need to build our base
        BuildingList buildList(_roomObj);

        // Use the tree filler to place buildings and roads
        Result res;
        treefill::Generator tfg(_roomObj, posPlan, buildList, m_seed);
        tfg.Generate(_gbl, res);

        const float endMS = cpu.call<float>("getUsed");
        const float timeTaken = endMS - startMS;
        PROF_LOG(_gbl.dbg, "Planner::Plan took %.2f ms", timeTaken);

        res.Draw(_roomObj);
    }

    void Planner::GenerateMovementCost(const ai::object::Room& _roomObj, ai::object::Terrain& _outTerrain)
    {
        const ai::object::Terrain& roomTerrain = _roomObj.GetTerrain();

        // Setup from static environment
        for (int y = 0; y < utils::constants::ROOM_HEIGHT; ++y)
        {
            for (int x = 0; x < utils::constants::ROOM_WIDTH; ++x)
            {
                const uint8_t terrainMask = roomTerrain.Get(x, y);
                uint8_t terrainCost = ai::utils::constants::TERRAINMOVECOST_PLAIN;
                if (terrainMask == ai::utils::constants::TERRAIN_MASK_WALL)
                    terrainCost = ai::utils::constants::TERRAINMOVECOST_OBSTACLE;
                else if (terrainMask == ai::utils::constants::TERRAIN_MASK_SWAMP)
                    terrainCost = ai::utils::constants::TERRAIN_MASK_SWAMP;
                _outTerrain.Set(x, y, terrainCost);
            }
        }

        // Setup from structures
        const ai::object::Room::StructureArray& structures = _roomObj.GetStructures();
        for (const auto& structure : structures)
        {
            const ai::object::RoomPosition&       pos        = structure->GetPos();
            const ai::utils::constants::STRUCTURE structType = structure->GetType();
            switch (structType)
            {
                case ai::utils::constants::STRUCTURE::ROAD:
                {
                    _outTerrain.Set(pos.GetX(), pos.GetY(), ai::utils::constants::TERRAINMOVECOST_ROAD);
                    break;
                }
                case ai::utils::constants::STRUCTURE::RAMPART:
                {
                    const ai::object::StructureRampart& rampart = static_cast<const ai::object::StructureRampart&>(*structure);
                    if (!rampart.IsPublic() && !rampart.IsMine())
                        _outTerrain.Set(pos.GetX(), pos.GetY(), ai::utils::constants::TERRAINMOVECOST_OBSTACLE);
                    break;
                }
                default:
                {
                    if (std::binary_search(ai::utils::constants::STRUCTURE_OBSTACLES.begin(), ai::utils::constants::STRUCTURE_OBSTACLES.end(), structType))
                        _outTerrain.Set(pos.GetX(), pos.GetY(), ai::utils::constants::TERRAINMOVECOST_OBSTACLE);
                }
            }
        }

        // Setup from construction sites
        const ai::object::Room::ConstructionSiteArray& csites = _roomObj.GetConstructionSites();
        for (const auto& csite : csites)
        { 
            // Skip if object is not an obstacle
            if (!std::binary_search(ai::utils::constants::STRUCTURE_OBSTACLES.begin(), ai::utils::constants::STRUCTURE_OBSTACLES.end(), csite.GetType()))
                continue;

            const ai::object::RoomPosition& pos = csite.GetPos();
            _outTerrain.Set(pos.GetX(), pos.GetY(), ai::utils::constants::TERRAINMOVECOST_OBSTACLE);
        }
    }

    template <typename PointArray>
    void Planner::CalcCostToPoints(const ai::object::Terrain& _costTerrain, const PointArray& _p, ai::object::DistanceTerrain& _out)
    {
        std::queue<ai::object::RoomPosition> toAnalyze;    
        
        for (const ai::object::RoomPosition& curPoint : _p)
        {
            _out.Set(curPoint.GetX(), curPoint.GetY(), 0);
            toAnalyze.push(curPoint);
        }

        while (!toAnalyze.empty())
        {
            ai::object::RoomPosition toCheck = toAnalyze.front();
            toAnalyze.pop();

            const uint8_t x = toCheck.GetX();
            const uint8_t y = toCheck.GetY();

            ai::object::DistanceTerrain::Tile curCost = _out.Get(x, y);

            const uint8_t left  = (x == 0) ? 0 : x - 1;
            const uint8_t top   = (y == 0) ? 0 : y - 1;
            const uint8_t right = (x == ai::utils::constants::ROOM_WIDTH  - 1) ? ai::utils::constants::ROOM_WIDTH  - 1 : x + 1;
            const uint8_t bot   = (y == ai::utils::constants::ROOM_HEIGHT - 1) ? ai::utils::constants::ROOM_HEIGHT - 1 : y + 1;

            for (uint8_t coordY = top; coordY <= bot; ++coordY)
            {
                for (uint8_t coordX = left; coordX <= right; ++coordX)
                {
                    // Ignore obstacles
                    const ai::object::Terrain::Tile coordCost = _costTerrain.Get(coordX, coordY);
                    if (coordCost == ai::utils::constants::TERRAINMOVECOST_OBSTACLE)
                        continue;
                    
                    ai::object::DistanceTerrain::Tile newCost  = curCost + coordCost;
                    ai::object::DistanceTerrain::Tile prevCost = _out.Get(coordX, coordY);
                    if ((prevCost == ai::object::DistanceTerrain::UNPATHABLE) || (newCost < prevCost))
                    {
                        _out.Set(coordX, coordY, newCost);
                        toAnalyze.emplace(coordX, coordY);
                    }
                }
            }
        }
    }

    void Planner::DrawDistance(const ai::object::Room& _roomObj, const ai::object::DistanceTerrain& _dist)
    {
        const js::val& js_room = _roomObj.GetJSObj();
        const js::val& js_rv   = js_room["visual"];
        char txtBuf[1024];

        float fMin = std::numeric_limits<float>::max();
        float fMax = std::numeric_limits<float>::min();
        _dist.for_each([&](uint8_t x, uint8_t y, ai::object::DistanceTerrain::Tile v)
        {
            if (v == 0xffff)
                return;

            const float fV = v;
            fMax = std::max(fMax, fV);
            fMin = std::min(fMin, fV);
        });
        const float fDelta    = fMax - fMin;
        const float fInvDelta = 1.0f / fDelta;

        _dist.for_each([&](uint8_t x, uint8_t y, ai::object::DistanceTerrain::Tile v)
        {
#if 0
            snprintf(txtBuf, sizeof(txtBuf), "%u", v);
            js_rv.call<js::val>("text", std::string(txtBuf), x, y);
#else
            if (v == ai::object::DistanceTerrain::UNPATHABLE)
                return;
            
            const float   fV = (v - fMin) * fInvDelta;
            const uint8_t nv = static_cast<uint8_t>(fV * 255.0f);

            snprintf(txtBuf, sizeof(txtBuf), "#%02x0000", nv);
            js::val js_style = js::val::object();
            js_style.set("fill", std::string(txtBuf));
            js_rv.call<js::val>("rect", x - 0.5f, y - 0.5f, 1, 1, js_style);
#endif
        });
    }

    void Planner::BuildPreferenceMap(ai::Global& _gbl, const ai::object::Room& _roomObj, const ai::object::Terrain& _mvmtCost, ai::object::DistanceTerrain& _out)
    {
        std::vector<ai::object::RoomPosition> interestPoints;

        // Gather all energy sources and minerals
        const std::vector<ai::object::EnergySource>& sources  = _roomObj.GetSources();
        const std::vector<ai::object::Mineral>&      minerals = _roomObj.GetMinerals();
        const size_t numInterestPoints = sources.size() + minerals.size();

        interestPoints.reserve(numInterestPoints);
        std::transform(sources.begin(),  sources.end(),  std::back_inserter(interestPoints), [](const auto& source ) { return source.GetPos();  });
        std::transform(minerals.begin(), minerals.end(), std::back_inserter(interestPoints), [](const auto& mineral) { return mineral.GetPos(); });

        // Find distance from each interest point to each tile
        std::vector<ai::object::DistanceTerrain> distToPoints(numInterestPoints, ai::object::DistanceTerrain(ai::object::DistanceTerrain::UNPATHABLE));
        for (size_t i = 0; i < numInterestPoints; ++i)
            CalcCostToPoint(_mvmtCost, interestPoints[i], distToPoints[i]);
        
        // TODO Find hostile exits - we should try to build away from them

        // Sum all distances in a combined structure
        _out.for_each([&](uint8_t _x, uint8_t _y, ai::object::DistanceTerrain::Tile& _v)
        {
            _v = std::accumulate(distToPoints.begin(), distToPoints.end(), 0, [=](const ai::object::DistanceTerrain::Tile& _prev, const ai::object::DistanceTerrain& _t)
            {
                const ai::object::DistanceTerrain::Tile& cur = _t.Get(_x, _y);
                return (cur == ai::object::DistanceTerrain::UNPATHABLE) ? ai::object::DistanceTerrain::UNPATHABLE : cur + _prev;
            });
        });

        //DrawDistance(_roomObj, _out);
    }
}
