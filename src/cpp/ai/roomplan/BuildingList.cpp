#include "BuildingList.h"

#include <algorithm>

#include "ai/object/Room.h"

namespace ai::roomplan
{
    void BuildingList::GenerateList()
    {
        // Fetch max quantity of each type of building        
        for (int structType = 0; structType < ai::utils::constants::STRUCTURE::STRUCTURE_COUNT; ++structType)
            m_buildList[structType] = ai::utils::constants::CONTROLLER_STRUCTURES[structType].back();

        // For each type, calc how many buildings are already built in this room, and remove them from the list
        const ai::object::Room::StructureArray& builtStructs = m_room.GetStructures();
        for (const auto& curStruct : builtStructs)
        {
            const ai::utils::constants::STRUCTURE structType = curStruct->GetType();
            if (m_buildList[structType] > 0)
                --m_buildList[structType];
        }

        // Do the same, with construction sites
        const ai::object::Room::ConstructionSiteArray& csites = m_room.GetConstructionSites();
        for (const auto& csite: csites)
        {
            const ai::utils::constants::STRUCTURE structType = csite.GetType();
            if (m_buildList[structType] > 0)
                --m_buildList[structType];
        }

        m_econBuildingsCount = 
            m_buildList[ai::utils::constants::STRUCTURE::OBSERVER]    +
            m_buildList[ai::utils::constants::STRUCTURE::NUKER]       +
            m_buildList[ai::utils::constants::STRUCTURE::POWER_SPAWN] +
            m_buildList[ai::utils::constants::STRUCTURE::TERMINAL]    +
            m_buildList[ai::utils::constants::STRUCTURE::STORAGE]     +
            m_buildList[ai::utils::constants::STRUCTURE::LAB]         +
            m_buildList[ai::utils::constants::STRUCTURE::SPAWN]       +
            m_buildList[ai::utils::constants::STRUCTURE::EXTENSION];
        
        // TODO: Better handle remote mining
        const unsigned int numResourceLinks  = m_room.GetSources().size() + m_room.GetMinerals().size();
        const unsigned int numAvailableLinks = m_buildList[ai::utils::constants::STRUCTURE::LINK];
        m_baseLinkCount = (numResourceLinks > numAvailableLinks) ? 0 : numAvailableLinks - numResourceLinks;
    }
}