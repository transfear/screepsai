#pragma once

#include "ai/object/RoomPosition.h"
#include "ai/object/Terrain.h"

namespace ai
{
    struct Global;
    namespace object { class Room; }

    namespace roomplan
    {
        class Planner
        {
            public:

                inline Planner(uint32_t _seed) : m_seed(_seed) { }

                void PlanAllRooms(ai::Global& _gbl);

                void Plan(ai::Global& _gbl, const ai::object::Room& _roomObj);

            private:

                void GenerateMovementCost(const ai::object::Room& _roomObj, ai::object::Terrain& _outTerrain);
                template <typename PointArray>
                void CalcCostToPoints(const ai::object::Terrain& _costTerrain, const PointArray& _p, ai::object::DistanceTerrain& _out);
                inline void CalcCostToPoint(const ai::object::Terrain& _costTerrain, const ai::object::RoomPosition& _p, ai::object::DistanceTerrain& _out);
                void DrawDistance(const ai::object::Room& _roomObj, const ai::object::DistanceTerrain& _dist);

                void BuildPreferenceMap(ai::Global& _gbl, const ai::object::Room& _roomObj, const ai::object::Terrain& _mvmtCost, ai::object::DistanceTerrain& _out);

                uint32_t m_seed;
        };

        inline void Planner::CalcCostToPoint(const ai::object::Terrain& _costTerrain, const ai::object::RoomPosition& _p, ai::object::DistanceTerrain& _out)
        {
            const std::array<ai::object::RoomPosition, 1> rp = { _p };
            CalcCostToPoints(_costTerrain, rp, _out);
        }
    }
}
