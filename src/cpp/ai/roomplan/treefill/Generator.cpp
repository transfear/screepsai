#include "Generator.h"

#include <algorithm>
#include <array>

#include "ai/dbg/Assert.h"
#include "ai/Global.h"
#include "ai/object/Room.h"
#include "ai/object/Terrain.h"
#include "ai/roomplan/BuildingList.h"
#include "ai/roomplan/Result.h"
#include "ai/roomplan/SortedPosPlan.h"

namespace ai::roomplan::treefill
{
    // true == road, false == building
    constexpr int PATTERN_DIM = 10;
    constexpr std::array<std::array<bool, PATTERN_DIM>, PATTERN_DIM> TREEFILL_PATTERN =
    {{
        { true,  false, false, false, false, false, false, false, false, false },
        { false, true,  false, false, true,  false, true,  false, false, true  },
        { false, false, true,  true,  false, false, false, true,  true,  false },
        { false, false, true,  true,  false, false, false, true,  true,  false },
        { false, true,  false, false, true,  false, true,  false, false, true  },
        { false, false, false, false, false, true,  false, false, false, false },
        { false, true,  false, false, true,  false, true,  false, false, true  },
        { false, false, true,  true,  false, false, false, true,  true,  false },
        { false, false, true,  true,  false, false, false, true,  true,  false },
        { false, true,  false, false, true,  false, true,  false, false, true  }
    }};

    constexpr std::array<ai::utils::constants::STRUCTURE, 8> STRUCTURE_PRIORITY =
    {
        ai::utils::constants::STRUCTURE::OBSERVER,
        ai::utils::constants::STRUCTURE::NUKER,
        ai::utils::constants::STRUCTURE::POWER_SPAWN,
        ai::utils::constants::STRUCTURE::TERMINAL,
        ai::utils::constants::STRUCTURE::STORAGE,
        ai::utils::constants::STRUCTURE::LAB,
        ai::utils::constants::STRUCTURE::SPAWN,
        ai::utils::constants::STRUCTURE::EXTENSION
    };

    constexpr int WALL_RANGE     = 1;
    constexpr int RESOURCE_RANGE = 2;

    void Generator::Generate(ai::Global& _gbl, ai::roomplan::Result& _out)
    {
        m_sortedPosCursor = 0;

        // Fetch center, put a road on it
        ai::object::RoomPosition startPoint = FetchNextBestRandomPos(_gbl);
        m_centerX = startPoint.GetX();
        m_centerY = startPoint.GetY();
        _out.GetRoads().emplace_back(ai::utils::constants::ROAD, startPoint);

        // Place buildings
        PlaceBuildings(_gbl, _out);
    }

    void Generator::PlaceBuildings(ai::Global& _gbl, ai::roomplan::Result& _out)
    {
        ai::roomplan::Result::EntryArray& outBuildings = _out.GetBuildings();
        outBuildings.clear();
        outBuildings.reserve(m_buildList.GetEconBuildingCount() + m_buildList.GetBaseLinkCount());

        // Loop on all buildings types, according to their priority
        for (ai::utils::constants::STRUCTURE type : STRUCTURE_PRIORITY)
        {
            uint16_t toPlace = m_buildList.GetBuildingCount(type);
            while (toPlace > 0)
            {
                PlaceBuilding(_gbl, type, _out);
                --toPlace;
            }
        }

        // TODO: Labs

        // TODO: Links

        // TODO: Towers

        // TODO: Containers
    }

    void Generator::PlaceBuilding(ai::Global& _gbl, ai::utils::constants::STRUCTURE& type, ai::roomplan::Result& _out)
    {
        bool bFoundLocation = false;
        do
        {
            // Look for next best position
            ai::object::RoomPosition destPos = FetchNextBestRandomPos(_gbl);

            // Check if location is suitable for a building
            // Check if location is near a wall
            // Check if location is near a resource. We'll typically have a lot of traffic there
            if (IsRoadLocation(destPos) || IsNextToWall(destPos) || IsNextToResource(destPos))
            {
                // TODO: don't do this if it's a container location!
                // In such case, build a road
                _out.GetRoads().emplace_back(ai::utils::constants::ROAD, destPos);
                continue;
            }

            _out.GetBuildings().emplace_back(type, destPos);
            bFoundLocation = true;
        }
        while (!bFoundLocation);
    }

    bool Generator::IsNextToResource(ai::object::RoomPosition _pos) const
    {
        const int pX = _pos.GetX();
        const int pY = _pos.GetY();

        const ai::object::Room::SourceArray&  sources  = m_room.GetSources();
        const ai::object::Room::MineralArray& minerals = m_room.GetMinerals();

        const auto isCloseToResource = [=](const auto& s)
        {
            const ai::object::RoomPosition& sp = s.GetPos();
            const int sX = sp.GetX();
            if (abs(sX - pX) > RESOURCE_RANGE)
                return false;
            
            const int sY = sp.GetY();
            return abs(sY - pY) <= RESOURCE_RANGE;
        };

        if (std::any_of(sources.begin(), sources.end(), isCloseToResource))
            return true;

        return std::any_of(minerals.begin(), minerals.end(), isCloseToResource);
    }

    bool Generator::IsNextToWall(ai::object::RoomPosition _pos) const
    {
        const ai::object::Terrain& terrain = m_room.GetTerrain();
        
        const int x = _pos.GetX();
        const int y = _pos.GetY();

        const int left  = std::max(x - WALL_RANGE, 0);
        const int right = std::min(x + WALL_RANGE, ai::utils::constants::ROOM_WIDTH  - 1);
        const int top   = std::max(y - WALL_RANGE, 0);
        const int bot   = std::min(y + WALL_RANGE, ai::utils::constants::ROOM_HEIGHT - 1);

        for (int curY = top; curY <= bot; ++curY)
        {
            for (int curX = left; curX <= right; ++curX)
            {
                const auto tile = terrain.Get(curX, curY);
                if (tile == ai::utils::constants::TERRAIN_MASK_WALL)
                    return true;
            }
        }

        return false;
    }

    bool Generator::IsRoadLocation(ai::object::RoomPosition _pos) const
    {
        const int posX = _pos.GetX();
        const int posY = _pos.GetY();

        int deltaX = (posX - m_centerX) % PATTERN_DIM;
        int deltaY = (posY - m_centerY) % PATTERN_DIM;
        if (deltaX < 0)
            deltaX += PATTERN_DIM;
        if (deltaY < 0)
            deltaY += PATTERN_DIM;
        
        const bool isRoad = TREEFILL_PATTERN[deltaY][deltaX];
        return isRoad;
    }

    ai::object::RoomPosition Generator::FetchNextBestRandomPos(ai::Global& _gbl)
    {
        ai::roomplan::SortedPosPlan::EntryArray& entries = m_posPlan.GetEntries();
        const size_t numEntries   = entries.size();
        const auto&  entriesStart = entries.begin();
        const auto&  entriesEnd   = entries.end();
        AI_ASSERT(_gbl.dbg, m_sortedPosCursor < numEntries);

        const uint16_t startIdx = m_sortedPosCursor;
        const uint16_t score    = entries[startIdx].first;

        // Find first entry which has a different priority to current cursor
        const auto& endIt = std::find_if(entriesStart + startIdx, entriesEnd, [=](const auto& e) { return e.first != score; });
        const uint16_t endIdx = (endIt == entriesEnd) ? numEntries - 1 : std::distance(entriesStart, endIt);

        // Select a random entry of equal score
        const uint16_t intervalSize = endIdx - startIdx;
        const uint16_t entryToUse   = ((m_seed++) % intervalSize) + startIdx;

        // Swap with first possibility, and fetch it
        std::swap(entries[startIdx], entries[entryToUse]);
        return FetchNextBestPos(_gbl);
    }

    ai::object::RoomPosition Generator::FetchNextBestPos(ai::Global& _gbl)
    {
        const ai::roomplan::SortedPosPlan::EntryArray& entries = m_posPlan.GetEntries();
        const uint16_t toFetch = m_sortedPosCursor++;
        AI_ASSERT(_gbl.dbg, toFetch < entries.size());

        return entries[toFetch].second;
    }
}