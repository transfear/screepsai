#pragma once

#include <cstdint>

#include "ai/object/RoomPosition.h"
#include "ai/utils/Constants.h"

namespace ai
{
    struct Global;
    namespace object { class Room; }

    namespace roomplan
    {
        class BuildingList;
        class Result;
        class SortedPosPlan;

        namespace treefill
        {
            class Generator
            {
                public:
                    inline Generator(const ai::object::Room& _room, ai::roomplan::SortedPosPlan& _posPlan, const ai::roomplan::BuildingList& _list, uint32_t& seed) 
                    : m_room(_room), m_buildList(_list), m_posPlan(_posPlan), m_seed(seed)
                    { }

                    void Generate(ai::Global& _gbl, ai::roomplan::Result& _out);

                private:

                    ai::object::RoomPosition FetchNextBestPos(ai::Global& _gbl);
                    ai::object::RoomPosition FetchNextBestRandomPos(ai::Global& _gbl);

                    void PlaceBuildings(ai::Global& _gbl, ai::roomplan::Result& _out);
                    void PlaceBuilding(ai::Global& _gbl, ai::utils::constants::STRUCTURE& type, ai::roomplan::Result& _out);
                    bool IsRoadLocation(ai::object::RoomPosition _pos) const;
                    bool IsNextToWall(ai::object::RoomPosition _pos) const;
                    bool IsNextToResource(ai::object::RoomPosition _pos) const;

                    const ai::object::Room& m_room;
                    const ai::roomplan::BuildingList&  m_buildList;
                    ai::roomplan::SortedPosPlan& m_posPlan;
                    uint32_t& m_seed;

                    uint16_t m_sortedPosCursor;
                    int      m_centerX;
                    int      m_centerY;

            };
        }
    }
}