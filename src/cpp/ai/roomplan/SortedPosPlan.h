#pragma once

#include "ai/object/RoomPosition.h"
#include "ai/object/Terrain.h"

namespace ai
{
    struct Global;
    namespace object { class Room; }

    namespace roomplan
    {
        class SortedPosPlan
        {
            public:
                using Entry      = std::pair<uint16_t, ai::object::RoomPosition>;
                using EntryArray = std::vector<Entry>;

                inline SortedPosPlan(const ai::object::Room& _room) : m_room (_room) { }

                void FromPrefMap(const ai::object::DistanceTerrain& _prefMap);
                void Draw() const;

                inline const EntryArray& GetEntries() const { return m_entries; }
                inline       EntryArray& GetEntries()       { return m_entries; }

            private:

                const ai::object::Room& m_room;
                EntryArray              m_entries;
        };
    }
}
