#include "SortedPosPlan.h"

#include <algorithm>

#include "ai/object/Room.h"
#include "ai/utils/Constants.h"

namespace ai::roomplan
{
    void SortedPosPlan::FromPrefMap(const ai::object::DistanceTerrain& _prefMap)
    {
        constexpr int maxTiles = ai::utils::constants::ROOM_WIDTH * ai::utils::constants::ROOM_HEIGHT;

        m_entries.clear();
        m_entries.reserve(maxTiles);

        _prefMap.for_each([&](uint8_t x, uint8_t y, ai::object::DistanceTerrain::Tile v)
        {
            if (v != ai::object::DistanceTerrain::UNPATHABLE)
                m_entries.emplace_back(std::piecewise_construct, std::forward_as_tuple(v), std::forward_as_tuple(x, y));
        });

        std::sort(m_entries.begin(), m_entries.end(), [](const Entry& a, const Entry& b){ return a.first < b.first; });
    }

    void SortedPosPlan::Draw() const
    {
        const js::val& js_room = m_room.GetJSObj();
        const js::val& js_rv   = js_room["visual"];

        const float fNumEntries = m_entries.size();
        const float fColorDelta = 255.0f / (fNumEntries - 1.0f);

        char txtBuf[1024];
        float fCurColor = 0.0f;
        for (const auto& entry : m_entries)
        {
            const uint8_t red = static_cast<uint8_t>(fCurColor);
            fCurColor += fColorDelta;

            snprintf(txtBuf, sizeof(txtBuf), "#%02x0000", red);
            js::val js_style = js::val::object();
            js_style.set("fill", std::string(txtBuf));
            js_rv.call<js::val>("rect", entry.second.GetX() - 0.5f, entry.second.GetY() - 0.5f, 1, 1, js_style);
        }
    }
}