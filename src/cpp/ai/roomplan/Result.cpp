#include "Result.h"

#include <array>
#include <string>
#include <string_view>
#include <unordered_map>
#include "ai/object/Room.h"

namespace ai::roomplan
{
    struct BuildingStyle
    {
        std::string_view fill;
        std::string_view stroke;

        float opacity;
        float radius;
    };

    constexpr std::array<BuildingStyle, ai::utils::constants::STRUCTURE::STRUCTURE_COUNT> BUILDING_STYLE =
    {{
        { "#f5cb42", "#ffff00", 0.4f,  0.45f }, // SPAWN
        { "#fcf047", "#ffff00", 0.4f,  0.35f }, // EXTENSION
        { "#b5b5b5", "#000000", 0.35f, 0.33f }, // ROAD
        { "#4f4f4f", "#000000", 0.4f,  0.45f }, // WALL
        { "#128f10", "#ff0000", 0.4f,  0.45f }, // RAMPART
        { "#a33965", "#ff00ff", 0.4f,  0.45f }, // KEEPER_LAIR
        { "#7239a3", "#0000ff", 0.4f,  0.35f }, // PORTAL
        { "#000000", "#ffffff", 0.4f,  0.45f }, // CONTROLLER
        { "#80aaff", "#ffff00", 0.4f,  0.35f }, // LINK
        { "#39d600", "#ffff00", 0.4f,  0.45f }, // STORAGE
        { "#d12a00", "#ff0000", 0.4f,  0.45f }, // TOWER
        { "#4600d1", "#0000ff", 0.4f,  0.40f }, // OBSERVER
        { "#aed100", "#00ffff", 0.4f,  0.45f }, // POWER_BANK
        { "#91910c", "#00ffff", 0.4f,  0.45f }, // POWER_SPAWN
        { "#4ca66f", "#00ffff", 0.4f,  0.45f }, // EXTRACTOR
        { "#ffffff", "#00ffff", 0.4f,  0.45f }, // LAB
        { "#c25508", "#ffff00", 0.4f,  0.45f }, // TERMINAL
        { "#888fb5", "#ffff00", 0.4f,  0.35f }, // CONTAINER
        { "#ff1000", "#ff0000", 0.4f,  0.45f }  // NUKER
    }};

    void Result::Draw(const ai::object::Room& _room) const
    {
        const js::val& js_room = _room.GetJSObj();
        const js::val& js_rv   = js_room["visual"];

        // Create a map for our building styles
        // TODO: do this only once!
        std::unordered_map<ai::utils::constants::STRUCTURE, js::val> buildingStyle;
        for (int structType = 0; structType < ai::utils::constants::STRUCTURE::STRUCTURE_COUNT; ++structType)
        {
            const BuildingStyle& bStyle = BUILDING_STYLE[structType];
            auto it = buildingStyle.emplace_hint(buildingStyle.end(), static_cast<ai::utils::constants::STRUCTURE>(structType), js::val::object());
            it->second.set("fill",    std::string(bStyle.fill));
            it->second.set("stroke",  std::string(bStyle.stroke));
            it->second.set("radius",  bStyle.radius);
            it->second.set("opacity", bStyle.opacity);
        }
        
        // Colorize our buildings
        for (const auto& entry : m_buildings)
        {
            js::val js_style = buildingStyle.find(entry.first)->second;
            js_rv.call<js::val>("circle", entry.second.GetX(), entry.second.GetY(), js_style);
        }

        // Color our roads
        js::val js_road = buildingStyle.find(ai::utils::constants::STRUCTURE::ROAD)->second;
        for (const auto& entry : m_roads)
            js_rv.call<js::val>("circle", entry.second.GetX(), entry.second.GetY(), js_road);
    }
}