#pragma once

#include "ai/utils/Constants.h"

namespace ai
{
    namespace object { class Room; }

    namespace roomplan
    {
        class BuildingList
        {
            public:
                inline BuildingList(const ai::object::Room& _room) : m_room(_room) { GenerateList(); }

                inline uint16_t GetBuildingCount(ai::utils::constants::STRUCTURE _type) const { return m_buildList[_type]; }
                inline uint16_t GetEconBuildingCount() const { return m_econBuildingsCount; }
                inline uint16_t GetBaseLinkCount()     const { return m_baseLinkCount; }


            private:
                void GenerateList();

                const ai::object::Room& m_room;

                std::array<uint16_t, ai::utils::constants::STRUCTURE::STRUCTURE_COUNT> m_buildList;

                uint16_t m_econBuildingsCount;
                uint16_t m_baseLinkCount;
        };
    }
}