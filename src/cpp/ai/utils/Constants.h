#pragma once

#include <array>
#include <string>
#include <limits>

namespace ai::utils::constants
{
    // Not in official Screeps API but constant anyways
    constexpr int ROOM_WIDTH  = 50;
    constexpr int ROOM_HEIGHT = 50;
    
    constexpr int ROOM_MAX_LEVEL = 8;

    constexpr int TERRAIN_MASK_NONE = 0;

    constexpr uint8_t TERRAINMOVECOST_ROAD     =   1;
    constexpr uint8_t TERRAINMOVECOST_PLAIN    =   2;
    constexpr uint8_t TERRAINMOVECOST_SWAMP    =  10;
    constexpr uint8_t TERRAINMOVECOST_OBSTACLE = 255;

    // Source: https://docs.screeps.com/api/#Constants

    constexpr int OK                        =  0;
    constexpr int ERR_NOT_OWNER             = -1;
    constexpr int ERR_NO_PATH               = -2;
    constexpr int ERR_NAME_EXISTS           = -3;
    constexpr int ERR_BUSY                  = -4;
    constexpr int ERR_NOT_FOUND             = -5;
    constexpr int ERR_NOT_ENOUGH_ENERGY     = -6;
    constexpr int ERR_NOT_ENOUGH_RESOURCES  = -6;
    constexpr int ERR_INVALID_TARGET        = -7;
    constexpr int ERR_FULL                  = -8;
    constexpr int ERR_NOT_IN_RANGE          = -9;
    constexpr int ERR_INVALID_ARGS          = -10;
    constexpr int ERR_TIRED                 = -11;
    constexpr int ERR_NO_BODYPART           = -12;
    constexpr int ERR_NOT_ENOUGH_EXTENSIONS = -6;
    constexpr int ERR_RCL_NOT_ENOUGH        = -14;
    constexpr int ERR_GCL_NOT_ENOUGH        = -15;

    constexpr int FIND_EXIT_TOP                   =    1;
    constexpr int FIND_EXIT_RIGHT                 =    3;
    constexpr int FIND_EXIT_BOTTOM                =    5;
    constexpr int FIND_EXIT_LEFT                  =    7;
    constexpr int FIND_EXIT                       =   10;
    constexpr int FIND_CREEPS                     =  101;
    constexpr int FIND_MY_CREEPS                  =  102;
    constexpr int FIND_HOSTILE_CREEPS             =  103;
    constexpr int FIND_SOURCES_ACTIVE             =  104;
    constexpr int FIND_SOURCES                    =  105;
    constexpr int FIND_DROPPED_ENERGY             = -106;
    constexpr int FIND_DROPPED_RESOURCES          =  106;
    constexpr int FIND_STRUCTURES                 =  107;
    constexpr int FIND_MY_STRUCTURES              =  108;
    constexpr int FIND_HOSTILE_STRUCTURES         =  109;
    constexpr int FIND_FLAGS                      =  110;
    constexpr int FIND_CONSTRUCTION_SITES         =  111;
    constexpr int FIND_MY_SPAWNS                  =  112;
    constexpr int FIND_HOSTILE_SPAWNS             =  113;
    constexpr int FIND_MY_CONSTRUCTION_SITES      =  114;
    constexpr int FIND_HOSTILE_CONSTRUCTION_SITES =  115;
    constexpr int FIND_MINERALS                   =  116;
    constexpr int FIND_NUKES                      =  117;
    constexpr int FIND_TOMBSTONES                 =  118;
    constexpr int FIND_POWER_CREEPS               =  119;
    constexpr int FIND_MY_POWER_CREEPS            =  120;
    constexpr int FIND_HOSTILE_POWER_CREEPS       =  121;

    constexpr int TOP          = 1;
    constexpr int TOP_RIGHT    = 2;
    constexpr int RIGHT        = 3;
    constexpr int BOTTOM_RIGHT = 4;
    constexpr int BOTTOM       = 5;
    constexpr int BOTTOM_LEFT  = 6;
    constexpr int LEFT         = 7;
    constexpr int TOP_LEFT     = 8;

    constexpr int COLOR_RED    = 1;
    constexpr int COLOR_PURPLE = 2;
    constexpr int COLOR_BLUE   = 3;
    constexpr int COLOR_CYAN   = 4;
    constexpr int COLOR_GREEN  = 5;
    constexpr int COLOR_YELLOW = 6;
    constexpr int COLOR_ORANGE = 7;
    constexpr int COLOR_BROWN  = 8;
    constexpr int COLOR_GREY   = 9;
    constexpr int COLOR_WHITE  = 10;

    constexpr std::string_view LOOK_CREEPS             = "creep";
    constexpr std::string_view LOOK_ENERGY             = "energy";
    constexpr std::string_view LOOK_RESOURCES          = "resource";
    constexpr std::string_view LOOK_SOURCES            = "source";
    constexpr std::string_view LOOK_MINERALS           = "mineral";
    constexpr std::string_view LOOK_STRUCTURES         = "structure";
    constexpr std::string_view LOOK_FLAGS              = "flag";
    constexpr std::string_view LOOK_CONSTRUCTION_SITES = "constructionSite";
    constexpr std::string_view LOOK_NUKES              = "nuke";
    constexpr std::string_view LOOK_TERRAIN            = "terrain";
    constexpr std::string_view LOOK_TOMBSTONES         = "tombstone";
    constexpr std::string_view LOOK_POWER_CREEPS       = "powerCreep";

    // TODO: OBSTACLE_OBJECT_TYPES

    enum BODYPART
    {
        MOVE = 0,
        WORK,
        CARRY,
        ATTACK,
        RANGED_ATTACK,
        TOUGH,
        HEAL,
        CLAIM,

        BODYPART_COUNT,
        BODYPART_NONE = BODYPART_COUNT
    };

    constexpr std::array<std::string_view, BODYPART::BODYPART_COUNT> BODYPART_NAME = { "move", "work", "carry", "attack", "ranged_attack", "tough", "heal", "claim" };
    constexpr std::array<int, BODYPART::BODYPART_COUNT> BODYPART_COST = { 50, 100, 80, 50, 250, 150, 10, 600 };

    constexpr int   CREEP_LIFE_TIME       = 1500;
    constexpr int   CREEP_CLAIM_LIFE_TIME = 600;
    constexpr float CREEP_CORPSE_RATE     = 0.2f;
    constexpr int   CREEP_PART_MAX_ENERGY = 125;

    constexpr int   CARRY_CAPACITY           = 50;
    constexpr int   HARVEST_POWER            = 2;
    constexpr int   HARVEST_MINERAL_POWER    = 1;
    constexpr int   REPAIR_POWER             = 100;
    constexpr int   DISMANTLE_POWER          = 50;
    constexpr int   BUILD_POWER              = 5;
    constexpr int   ATTACK_POWER             = 30;
    constexpr int   UPGRADE_CONTROLLER_POWER = 1;
    constexpr int   RANGED_ATTACK_POWER      = 10;
    constexpr int   HEAL_POWER               = 12;
    constexpr int   RANGED_HEAL_POWER        = 4;
    constexpr float REPAIR_COST              = 0.01f;
    constexpr float DISMANTLE_COST           = 0.005f;

    constexpr int RAMPART_DECAY_AMOUNT = 300;
    constexpr int RAMPART_DECAY_TIME   = 100;
    constexpr int RAMPART_HITS         = 1;
    constexpr std::array<int, ROOM_MAX_LEVEL> RAMPART_HITS_MAX = { 0, 300000, 1000000, 3000000, 10000000, 30000000, 100000000, 300000000 };

    constexpr int ENERGY_REGEN_TIME = 300;
    constexpr int ENERGY_DECAY      = 1000;

    constexpr int   SPAWN_HITS            = 5000;
    constexpr int   SPAWN_ENERGY_START    = 300;
    constexpr int   SPAWN_ENERGY_CAPACITY = 300;
    constexpr int   CREEP_SPAWN_TIME      = 3;
    constexpr float SPAWN_RENEW_RATIO     = 1.2f;

    constexpr int SOURCE_ENERGY_CAPACITY         = 3000;
    constexpr int SOURCE_ENERGY_NEUTRAL_CAPACITY = 1500;
    constexpr int SOURCE_ENERGY_KEEPER_CAPACITY  = 4000;

    constexpr int WALL_HITS     = 1;
    constexpr int WALL_HITS_MAX = 300000000;

    constexpr int EXTENSION_HITS = 1000;
    constexpr std::array<int, ROOM_MAX_LEVEL> EXTENSION_ENERGY_CAPACITY = { 50, 50, 50, 50, 50, 50, 100, 200 };

    constexpr int ROAD_HITS                = 5000;
    constexpr int ROAD_WEAROUT             = 1;
    constexpr int ROAD_WEAROUT_POWER_CREEP = 100;
    constexpr int ROAD_DECAY_AMOUNT        = 100;
    constexpr int ROAD_DECAY_TIME          = 1000;

    constexpr int   LINK_HITS       = 1000;
    constexpr int   LINK_HITS_MAX   = 1000;
    constexpr int   LINK_CAPACITY   = 800;
    constexpr int   LINK_COOLDOWN   = 1;
    constexpr float LINK_LOSS_RATIO = 0.03f;

    constexpr int STORAGE_CAPACITY = 1000000;
    constexpr int STORAGE_HITS     = 10000;

    enum STRUCTURE
    {
        SPAWN = 0,
        EXTENSION,
        ROAD,
        WALL,
        RAMPART,
        KEEPER_LAIR,
        PORTAL,
        CONTROLLER,
        LINK,
        STORAGE,
        TOWER,
        OBSERVER,
        POWER_BANK,
        POWER_SPAWN,
        EXTRACTOR,
        LAB,
        TERMINAL,
        CONTAINER,
        NUKER,

        STRUCTURE_COUNT,
        STRUCTURE_NONE = STRUCTURE_COUNT,
    };

    constexpr std::array<STRUCTURE, 13> STRUCTURE_OBSTACLES = { STRUCTURE::SPAWN, STRUCTURE::EXTENSION, STRUCTURE::WALL, STRUCTURE::CONTROLLER, STRUCTURE::LINK, STRUCTURE::STORAGE, STRUCTURE::TOWER, STRUCTURE::OBSERVER, STRUCTURE::POWER_BANK, STRUCTURE::POWER_SPAWN, STRUCTURE::LAB, STRUCTURE::TERMINAL, STRUCTURE::NUKER };
    
    constexpr std::array<std::string_view, STRUCTURE::STRUCTURE_COUNT> STRUCTURE_NAME = { "spawn", "extension", "road", "constructedWall", "rampart", "keeperLair", "portal", "controller", "link", "storage", "tower", "observer", "powerBank", "powerSpawn", "extractor", "lab", "terminal", "container", "nuker" };
    constexpr std::array<int, STRUCTURE::STRUCTURE_COUNT> STRUCTURE_COST = { 15000, 3000, 300, 1, 1, std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), 5000, 30000, 5000, 8000, std::numeric_limits<int>::max(), 100000, 5000, 50000, 100000, 5000, 100000 };

    constexpr int CONSTRUCTION_COST_ROAD_SWAMP_RATIO = 5;
    constexpr int CONSTRUCTION_COST_ROAD_WALL_RATIO  = 150;

    constexpr std::array<int, ROOM_MAX_LEVEL> CONTROLLER_LEVELS = { 200, 45000, 135000, 405000, 1215000, 3645000, 10935000, std::numeric_limits<int>::max() };

    constexpr std::array<std::array<int, ROOM_MAX_LEVEL>, STRUCTURE::STRUCTURE_COUNT> CONTROLLER_STRUCTURES =
    {{
        {    1,    1,    1,    1,    1,    1,    2,    3 }, // SPAWN
        {    0,    5,   10,   20,   30,   40,   50,   60 }, // EXTENSION
        { 2500, 2500, 2500, 2500, 2500, 2500, 2500, 2500 }, // ROAD
        {    0, 2500, 2500, 2500, 2500, 2500, 2500, 2500 }, // WALL
        {    0, 2500, 2500, 2500, 2500, 2500, 2500, 2500 }, // RAMPART
        {    0,    0,    0,    0,    0,    0,    0,    0 }, // KEEPER_LAIR
        {    0,    0,    0,    0,    0,    0,    0,    0 }, // PORTAL
        {    0,    0,    0,    0,    0,    0,    0,    0 }, // CONTROLLER
        {    0,    0,    0,    0,    2,    3,    4,    6 }, // LINK
        {    0,    0,    0,    1,    1,    1,    1,    1 }, // STORAGE
        {    0,    0,    1,    4,    2,    2,    3,    6 }, // TOWER
        {    0,    0,    0,    0,    0,    0,    0,    1 }, // OBSERVER
        {    0,    0,    0,    0,    0,    0,    0,    0 }, // POWER_BANK
        {    0,    0,    0,    0,    0,    0,    0,    1 }, // POWER_SPAWN
        {    0,    0,    0,    0,    0,    1,    1,    1 }, // EXTRACTOR
        {    0,    0,    0,    0,    0,    3,    6,   10 }, // LAB
        {    0,    0,    0,    0,    0,    1,    1,    1 }, // TERMINAL
        {    5,    5,    5,    5,    5,    5,    5,    5 }, // CONTAINER
        {    0,    0,    0,    0,    0,    0,    0,    1 }  // NUKER
    }};

    constexpr std::array<int, ROOM_MAX_LEVEL> CONTROLLER_DOWNGRADE = { 20000, 10000, 20000, 40000, 80000, 120000, 150000, 200000};
    constexpr int CONTROLLER_DOWNGRADE_RESTORE            = 100;
    constexpr int CONTROLLER_DOWNGRADE_SAFEMODE_THRESHOLD = 5000;
    constexpr int CONTROLLER_CLAIM_DOWNGRADE              = 300;
    constexpr int CONTROLLER_RESERVE                      = 1;
    constexpr int CONTROLLER_RESERVE_MAX                  = 5000;
    constexpr int CONTROLLER_MAX_UPGRADE_PER_TICK         = 15;
    constexpr int CONTROLLER_ATTACK_BLOCKED_UPGRADE       = 1000;
    constexpr int CONTROLLER_NUKE_BLOCKED_UPGRADE         = 200;

    constexpr int SAFE_MODE_DURATION = 20000;
    constexpr int SAFE_MODE_COOLDOWN = 50000;
    constexpr int SAFE_MODE_COST     = 1000;

    constexpr int   TOWER_HITS          = 3000;
    constexpr int   TOWER_CAPACITY      = 1000;
    constexpr int   TOWER_ENERGY_COST   = 10;
    constexpr int   TOWER_POWER_ATTACK  = 600;
    constexpr int   TOWER_POWER_HEAL    = 400;
    constexpr int   TOWER_POWER_REPAIR  = 800;
    constexpr int   TOWER_OPTIMAL_RANGE = 5;
    constexpr int   TOWER_FALLOFF_RANGE = 20;
    constexpr float TOWER_FALLOFF       = 0.75f;

    constexpr int OBSERVER_HITS  = 500;
    constexpr int OBSERVER_RANGE = 10;

    constexpr int   POWER_BANK_HITS          = 2000000;
    constexpr int   POWER_BANK_CAPACITY_MAX  = 5000;
    constexpr int   POWER_BANK_CAPACITY_MIN  = 500;
    constexpr float POWER_BANK_CAPACITY_CRIT = 0.3f;
    constexpr int   POWER_BANK_DECAY         = 5000;
    constexpr float POWER_BANK_HIT_BACK      = 0.5f;

    constexpr int EXTRACTOR_HITS     = 500;
    constexpr int EXTRACTOR_COOLDOWN = 5;

    constexpr int LAB_HITS             = 500;
    constexpr int LAB_MINERAL_CAPACITY = 3000;
    constexpr int LAB_ENERGY_CAPACITY  = 2000;
    constexpr int LAB_BOOST_ENERGY     = 20;
    constexpr int LAB_BOOST_MINERAL    = 30;
    constexpr int LAB_COOLDOWN         = 10;           // not used
    constexpr int LAB_REACTION_AMOUNT  = 5;
    constexpr int LAB_UNBOOST_ENERGY   = 0;
    constexpr int LAB_UNBOOST_MINERAL  = 15;

    constexpr float GCL_POW      = 2.4f;
    constexpr int   GCL_MULTIPLY = 1000000;
    constexpr int   GCL_NOVICE   = 3;

    // What is this??
    // MODE_SIMULATION: null,
    // MODE_WORLD: null,

    constexpr int TERRAIN_MASK_WALL  = 1;
    constexpr int TERRAIN_MASK_SWAMP = 2;
    constexpr int TERRAIN_MASK_LAVA  = 4;

    constexpr int MAX_CONSTRUCTION_SITES = 100;
    constexpr int MAX_CREEP_SIZE         = 50;

}
