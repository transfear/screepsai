#pragma once

namespace ai::utils
{
    // Source: https://codereview.stackexchange.com/questions/127925/typelist-with-extractor

    template <class... Types> class type_list {};

    template <std::size_t idx, class... Types>
    class extract
    {
        public:
            using type = typename extract_impl<0, idx, Types...>::type;

        private: 
            static_assert( idx < sizeof...( Types ), "index out of bounds" );

            template <std::size_t i, std::size_t n, class... Rest> struct extract_impl;

            template <std::size_t i, std::size_t n, class T, class... Rest>
            struct extract_impl<i, n, T, Rest...>
            {
                using type = typename extract_impl<i + 1, n, Rest...>::type;
            };

            template <std::size_t n, class T, class... Rest>
            struct extract_impl<n, n, T, Rest...>
            {
                using type = T;
            };
    };

    template <std::size_t idx, class TypeList> struct type_list_extract;

    template <std::size_t idx, template <class...> class TypeList, class... Types>
    struct type_list_extract<idx, TypeList<Types...>>
    {
        using type = typename extract<idx, Types...>::type;
    };

    template <std::size_t idx, class TypeList>
    using type_list_extract_t = typename type_list_extract<idx, TypeList>::type;
}