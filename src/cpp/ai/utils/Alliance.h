#pragma once

#include <algorithm>
#include <array>
#include <string>

namespace ai::utils::alliance
{
    inline bool IsAlliedPlayer(const std::string& _playerName)
    {
        static const std::array<std::string, 5> allies = 
        {
            "Adamant",
            "Arkastan",
            "Braykin08",
            "Broswendes",
            "Shadowist"
        };

        return std::binary_search(allies.begin(), allies.end(), _playerName);
    }
}
