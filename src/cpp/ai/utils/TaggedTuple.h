#pragma once
#include <tuple>

// Source: https://stackoverflow.com/questions/13065166/c11-tagged-tuple

namespace ai::utils::tagged_tuple
{
    template<typename... Ts> struct typelist 
    {
        template<typename T> using prepend = typelist<T, Ts...>;
    };

    template<typename T, typename... Ts> struct index;
    template<typename T, typename... Ts> struct index<T, T, Ts...>:
    std::integral_constant<int, 0> {};
    template<typename T, typename U, typename... Ts> struct index<T, U, Ts...>:
    std::integral_constant<int, index<T, Ts...>::value + 1> {};

    template<int n, typename... Ts> struct nth_impl;
    template<typename T, typename... Ts> struct nth_impl<0, T, Ts...>
    {
        using type = T;
    };
    template<int n, typename T, typename... Ts> struct nth_impl<n, T, Ts...> 
    {
        using type = typename nth_impl<n - 1, Ts...>::type; 
    };
    template<int n, typename... Ts> using nth = typename nth_impl<n, Ts...>::type;

    template<int n, int m, typename... Ts> struct extract_impl;
    template<int n, int m, typename T, typename... Ts>
    struct extract_impl<n, m, T, Ts...> : extract_impl<n, m - 1, Ts...> { };
    template<int n, typename T, typename... Ts> struct extract_impl<n, 0, T, Ts...>
    {
        using types = typename extract_impl<n, n - 1, Ts...>::types::template prepend<T>; 
    };
    template<int n, int m> struct extract_impl<n, m> 
    {
        using types = typelist<>; 
    };

    template<int n, int m, typename... Ts> using extract = typename extract_impl<n, m, Ts...>::types;

    template<typename S, typename T> struct tt_impl;
    template<typename... Ss, typename... Ts> struct tt_impl<typelist<Ss...>, typelist<Ts...>> : public std::tuple<Ts...>
    {
        template<typename... Args> tt_impl(Args &&...args) : std::tuple<Ts...>(std::forward<Args>(args)...) { }
        template<typename S> constexpr nth<index<S, Ss...>::value, Ts...>& get()
        {
            return std::get<index<S, Ss...>::value>(*this); 
        }
        template<typename S> constexpr const nth<index<S, Ss...>::value, Ts...>& get() const
        {
            return std::get<index<S, Ss...>::value>(*this); 
        }
    };

    template<typename... Ts> 
    class tagged_tuple : public tt_impl<extract<2, 0, Ts...>, extract<2, 1, Ts...>> 
    {
        public:
            template<typename... Args> 
            tagged_tuple(Args &&...args) : tt_impl<extract<2, 0, Ts...>, extract<2, 1, Ts...>>(std::forward<Args>(args)...) { }

            // Source: https://codereview.stackexchange.com/questions/173564/implementation-of-static-for-to-iterate-over-elements-of-stdtuple-using-c17
            template <class Func>
            constexpr void static_for_const(Func &&f) const
            {
                static_for_const_impl(std::forward<Func>(f), std::make_index_sequence<(sizeof...(Ts) / 2)>{});
            }
            template <class Func>
            constexpr void static_for(Func &&f)
            {
                static_for_impl(std::forward<Func>(f), std::make_index_sequence<(sizeof...(Ts) / 2)>{});
            }

        private:
            template <class Func, std::size_t ...Is>
            constexpr void static_for_const_impl(Func &&f, std::index_sequence<Is...> ) const
            {
                ( f(std::get<Is>(*this)),... );
            }
            template <class Func, std::size_t ...Is>
            constexpr void static_for_impl(Func &&f, std::index_sequence<Is...> )
            {
                ( f(std::get<Is>(*this)),... );
            }
    };
}
