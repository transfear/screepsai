#pragma once

#include <tuple>

namespace ai::utils
{
    // Source: https://stackoverflow.com/questions/18063451/get-index-of-a-tuple-elements-type

    template <class T, class Tuple> struct IndexOfType;

    template <class T, class... Types>
    struct IndexOfType<T, std::tuple<T, Types...>> 
    {
        static const std::size_t value = 0;
    };

    template <class T, class U, class... Types>
    struct IndexOfType<T, std::tuple<U, Types...>> 
    {
        static const std::size_t value = 1 + IndexOfType<T, std::tuple<Types...>>::value;
    };
}