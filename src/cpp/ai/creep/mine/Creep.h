#pragma once

#include "ai/serial/SerializableAggregate.h"
#include "ai/serial/SerializableField.h"
#include "ai/serial/object/Creep.h"
#include "ai/utils/TaggedTuple.h"

#include "Role.h"

namespace ai
{
    namespace object { class Creep; }

    namespace creep::mine
    {
        namespace CreepSRL
        {
            struct Creep {};
            struct Role  {};
            using Data = ai::utils::tagged_tuple::tagged_tuple
            <
                Creep, serial::object::Creep,
                Role,  serial::SerializableField<RoleType>
            >;
        }

        class Creep : public serial::SerializableAggregate<CreepSRL::Data>
        {
            public:
                inline Creep() { }
                Creep(const ai::object::Creep* _creepObj);

                inline const ai::object::Creep* GetObj() const { return m_data.get<CreepSRL::Creep>().getPtr(); }

                inline void AssignRole(RoleType _role) { m_data.get<CreepSRL::Role>() = _role; }
                inline RoleType GetRole() const        { return m_data.get<CreepSRL::Role>().get(); }
        };
    }
}
