#include "Role.h"

namespace ai::creep::mine
{
    const RoleTemplate g_roles[RoleType::Count] =
    {
        // Worker
        {
            { 
                ai::utils::constants::BODYPART::MOVE,
                ai::utils::constants::BODYPART::WORK,
                ai::utils::constants::BODYPART::CARRY
            },

            3
        },

        // Upgrader
        {
            { 
                ai::utils::constants::BODYPART::MOVE,
                ai::utils::constants::BODYPART::WORK,
                ai::utils::constants::BODYPART::CARRY,
            },

            3
        },

        // Hauler
        {
            { 
                ai::utils::constants::BODYPART::MOVE,
                ai::utils::constants::BODYPART::CARRY,
            },

            2
        }
    };
}
