#include "Creep.h"
#include "ai/object/Creep.h"

namespace ai::creep::mine
{
    Creep::Creep(const ai::object::Creep* _creepObj)
    : SerializableAggregate
    (
        std::forward_as_tuple(_creepObj, _creepObj->GetId()),
        RoleType::Free
    )
    {
    }
}
