#pragma once

#include "ai/utils/Constants.h"

namespace ai::creep::mine
{
    enum RoleType
    {
        Worker,
        Upgrader,
        Hauler,

        Count,
        Free = Count,
    };

    struct RoleTemplate
    {
        std::array<ai::utils::constants::BODYPART, ai::utils::constants::BODYPART::BODYPART_COUNT> parts;
        uint8_t numParts;
        uint8_t pad[3]; //unused
    };

    extern const RoleTemplate g_roles[RoleType::Count];
}
