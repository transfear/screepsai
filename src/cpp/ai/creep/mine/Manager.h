#pragma once

#include <string>
#include <unordered_map>

#include "ai/serial/SerializableAggregate.h"
#include "ai/serial/SerializableField.h"
#include "ai/utils/TaggedTuple.h"

#include "Creep.h"

namespace ai
{
    struct Global;
    namespace object { class Creep; }

    namespace creep::mine
    {
        using CreepMap = std::unordered_map<std::string, Creep>;
        namespace ManagerSRL
        {
            struct CreepMap { };
            using Data = ai::utils::tagged_tuple::tagged_tuple
            <
                CreepMap, serial::SerializableField<creep::mine::CreepMap>
            >;
        }

        class Manager : public serial::SerializableAggregate<ManagerSRL::Data>
        {
            public:
                inline Manager() { }

                void OnCreepAcquired(ai::object::Creep& _creepObj, ai::Global& _gbl);
                void OnCreepLost(const std::string& _creepId, ai::Global& _gbl);
        };
    }
}
