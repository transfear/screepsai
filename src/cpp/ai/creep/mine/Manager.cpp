#include "Manager.h"

#include "Role.h"
#include "ai/Global.h"
#include "ai/dbg/Assert.h"

namespace ai::creep::mine
{
    void Manager::OnCreepAcquired(ai::object::Creep& _creepObj, ai::Global& _gbl)
    {       
        const std::string& creepId = _creepObj.GetId();

        // Save it
        CreepMap& creepMap = m_data.get<ManagerSRL::CreepMap>().get();
        AI_ASSERT(_gbl.dbg, creepMap.find(creepId) == creepMap.end());

        auto it = creepMap.emplace_hint(creepMap.end(), creepId, &_creepObj);
        Creep& creep = it->second;

        // 1. Evaluate what the creep can do (to which role it is eligible)
        
        //const ai::object::CreepBody& body = _creepObj.GetBody();
        
        std::array<float, RoleType::Count> roleScores;
        for (int roleIdx = 0; roleIdx < RoleType::Count; ++roleIdx)
        {
            // TODO: Do not hardcode worker
            roleScores[roleIdx] = (roleIdx == RoleType::Worker) ? 1.0f : 0.0f;
        }
        const RoleType bestRole = RoleType::Worker;

        // TODO: Do not hardcode best room
        // 2. Evaluate rooms it can reach
        // 3. For these rooms, evaluate the most "in need" room for this creep
        // 4. Find the best <room, role> pair of this creep
        
        // const ai::object::Room& bestRoom = _creepObj.GetRoom();
        
        // 5. Notify this room's dependency that a new creep is available
        // TODO
        creep.AssignRole(bestRole);

    }
    
    void Manager::OnCreepLost(const std::string& _creepId, ai::Global& _gbl)
    {
        // Remove from the saved map
        CreepMap& creepMap = m_data.get<ManagerSRL::CreepMap>().get();
        auto it = creepMap.find(_creepId);
        AI_ASSERT(_gbl.dbg, it != creepMap.end());


        // 5. Notify this room's dependency that this creep is dead
        // TODO

        creepMap.erase(it);
    }
}
