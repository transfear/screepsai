#include "Global.h"

#include "ai/mem/GlobalCache.h"

namespace ai
{
    Global::Global() 
    : mem(*mem::g_cache.pMemoryState), myCreepMgr(*mem::g_cache.pCreepMgr), econMgr(*mem::g_cache.pEconMgr)
    { 
        
    }
}
