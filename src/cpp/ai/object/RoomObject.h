#pragma once

#include "RoomPosition.h"
#include "cppreeps/cppreeps.h"

namespace ai::object
{
    class Room;

    class RoomObject
    {
        public:
            RoomObject(const js::val& _js_obj, const Room& _room);

            inline const RoomPosition& GetPos() const { return m_pos; }
            inline const Room& GetRoom() const { return m_room; }

            inline const js::val& GetJSObj() const { return m_js_obj; }

        protected:
            const Room&  m_room;
            js::val      m_js_obj;
            RoomPosition m_pos;
            uint8_t      m_RoomObjectPad[2];  //unused
    };
}
