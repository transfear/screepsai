#include "Room.h"

#include "ai/object/StructureContainer.h"
#include "ai/object/StructureController.h"
#include "ai/object/StructureExtension.h"
#include "ai/object/StructureExtractor.h"
#include "ai/object/StructureKeeperLair.h"
#include "ai/object/StructureLab.h"
#include "ai/object/StructureLink.h"
#include "ai/object/StructureNuker.h"
#include "ai/object/StructureObserver.h"
#include "ai/object/StructurePortal.h"
#include "ai/object/StructurePowerBank.h"
#include "ai/object/StructurePowerSpawn.h"
#include "ai/object/StructureRampart.h"
#include "ai/object/StructureRoad.h"
#include "ai/object/StructureSpawn.h"
#include "ai/object/StructureStorage.h"
#include "ai/object/StructureTerminal.h"
#include "ai/object/StructureTower.h"
#include "ai/object/StructureWall.h"

#include "ai/utils/Constants.h"

namespace ai::object
{
    void Room::AddStructure(const js::val& _js_structure)
    {
        const auto structureNameIt = std::find(ai::utils::constants::STRUCTURE_NAME.begin(), ai::utils::constants::STRUCTURE_NAME.end(), _js_structure["structureType"].as<std::string>());
        const ai::utils::constants::STRUCTURE structureType = static_cast<ai::utils::constants::STRUCTURE>(structureNameIt - ai::utils::constants::STRUCTURE_NAME.begin());

        switch (structureType)
        {
            case ai::utils::constants::STRUCTURE::SPAWN:
            {
                m_structures.push_back(std::make_unique<StructureSpawn>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::EXTENSION:
            {
                m_structures.push_back(std::make_unique<StructureExtension>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::ROAD:
            {
                m_structures.push_back(std::make_unique<StructureRoad>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::WALL:
            {
                m_structures.push_back(std::make_unique<StructureWall>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::RAMPART:
            {
                m_structures.push_back(std::make_unique<StructureRampart>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::KEEPER_LAIR:
            {
                m_structures.push_back(std::make_unique<StructureKeeperLair>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::PORTAL:
            {
                m_structures.push_back(std::make_unique<StructurePortal>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::CONTROLLER:
            {
                m_structures.push_back(std::make_unique<StructureController>(*this, _js_structure));
                m_pController = static_cast<StructureController*>(m_structures.back().get());
                break;
            }

            case ai::utils::constants::STRUCTURE::LINK:
            {
                m_structures.push_back(std::make_unique<StructureLink>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::STORAGE:
            {
                m_structures.push_back(std::make_unique<StructureStorage>(*this, _js_structure));
                m_pStorage = static_cast<StructureStorage*>(m_structures.back().get());
                break;
            }

            case ai::utils::constants::STRUCTURE::TOWER:
            {
                m_structures.push_back(std::make_unique<StructureTower>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::OBSERVER:
            {
                m_structures.push_back(std::make_unique<StructureObserver>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::POWER_BANK:
            {
                m_structures.push_back(std::make_unique<StructurePowerBank>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::POWER_SPAWN:
            {
                m_structures.push_back(std::make_unique<StructurePowerSpawn>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::EXTRACTOR:
            {
                m_structures.push_back(std::make_unique<StructureExtractor>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::LAB:
            {
                m_structures.push_back(std::make_unique<StructureLab>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::TERMINAL:
            {
                m_structures.push_back(std::make_unique<StructureTerminal>(*this, _js_structure));
                m_pTerminal = static_cast<StructureTerminal*>(m_structures.back().get());
                break;
            }

            case ai::utils::constants::STRUCTURE::CONTAINER:
            {
                m_structures.push_back(std::make_unique<StructureContainer>(*this, _js_structure));
                break;
            }

            case ai::utils::constants::STRUCTURE::NUKER:
            {
                m_structures.push_back(std::make_unique<StructureNuker>(*this, _js_structure));
                break;
            }

            default:
            {
                // Shouldn't reach here!
                break;
            }
        }
    }
}
