#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureRampart : public OwnedStructure
    {
        public:
            inline StructureRampart(Room& _room, const js::val& _js_obj) 
            : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::RAMPART), m_isPublic(_js_obj["isPublic"].as<bool>()) { }

            inline bool IsPublic() const { return m_isPublic; }

        protected:
            bool m_isPublic;

    };
}
