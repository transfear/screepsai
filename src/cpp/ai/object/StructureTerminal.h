#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureTerminal : public OwnedStructure
    {
        public:
            inline StructureTerminal(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::TERMINAL) { }

        protected:

    };
}
