#include "EnergySource.h"
#include "Room.h"

#include "ai/utils/Constants.h"

namespace ai::object
{
   uint8_t EnergySource::GetHarvestableLocations()
   {
        if (m_harvestableLocations)
            return *m_harvestableLocations;

        const Terrain& terrain = m_room.GetTerrain();

        // Look around the mineral source to find empty spots where we could park a creep to harvest it
        uint8_t harvestableLocations = 0;

        const uint8_t x = m_pos.GetX();
        const uint8_t y = m_pos.GetY();

        const uint8_t topRow   = (y > 0) ? y - 1 : 0;
        const uint8_t botRow   = (y < ai::utils::constants::ROOM_HEIGHT - 1) ? y + 1 : ai::utils::constants::ROOM_HEIGHT - 1;
        const uint8_t leftCol  = (x > 0) ? x - 1 : 0;
        const uint8_t rightCol = (x < ai::utils::constants::ROOM_WIDTH  - 1) ? x + 1 : ai::utils::constants::ROOM_WIDTH - 1;

        for (int curY = topRow; curY <= botRow; ++curY)
        {
            for (int curX = leftCol; curX <= rightCol; ++curX)
            {
                const uint8_t terrainMask = terrain.Get(curX, curY);
                if ((terrainMask & (ai::utils::constants::TERRAIN_MASK_WALL|ai::utils::constants::TERRAIN_MASK_LAVA)) != 0)
                    continue;
                ++harvestableLocations;
            }
        }

        m_harvestableLocations = harvestableLocations;
        return harvestableLocations;
    }
}
