#pragma once

#include <optional>
#include <string>
#include "cppreeps/cppreeps.h"

#include "StructureBase.h"
#include "Terrain.h"
#include "EnergySource.h"
#include "Mineral.h"
#include "ConstructionSite.h"

namespace ai::object
{
    class StructureController;
    class StructureStorage;
    class StructureTerminal;

    enum class RoomFlags : uint8_t
    {
        kNone    = 0x00,
        kVisible = 0x01,
        kOwned   = 0x02
    };

    inline RoomFlags operator&(RoomFlags a, RoomFlags b)
    {
        return static_cast<RoomFlags>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
    }

    inline RoomFlags operator|(RoomFlags a, RoomFlags b)
    {
        return static_cast<RoomFlags>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
    }

    inline bool IsVisible(RoomFlags _flags)
    {
        return (_flags & RoomFlags::kVisible) != RoomFlags::kNone;
    }

    inline bool IsOwned(RoomFlags _flags)
    {
        return (_flags & RoomFlags::kOwned) != RoomFlags::kNone;
    }

    class Room
    {
        public:
            using SourceArray           = std::vector<EnergySource>;
            using MineralArray          = std::vector<Mineral>;
            using ConstructionSiteArray = std::vector<ConstructionSite>;
            using StructureArray        = std::vector<std::unique_ptr<StructureBase>>;

            inline Room(const js::val& _js_room, const std::string& _name, const RoomFlags& _flags) : m_js_obj(_js_room), m_name(_name), m_flags(_flags) { }
            void AddStructure(const js::val& _js_structure);
            inline void AddConstructionSite(const js::val& _js_csite);

            inline const Terrain&               GetTerrain()    const;
            inline const SourceArray&           GetSources()    const;
            inline const MineralArray&          GetMinerals()   const;
            inline const StructureArray&        GetStructures() const;
            inline const ConstructionSiteArray& GetConstructionSites() const;

            inline const js::val& GetJSObj() const { return m_js_obj; }

            inline const std::string& GetName() const { return m_name; }
            inline RoomFlags GetFlags()  const { return m_flags; }
            inline bool      IsVisible() const { return object::IsVisible(m_flags); }
            inline bool      IsOwned()   const { return object::IsOwned(m_flags); }

            inline const StructureController* GetController() const { return m_pController; }
            inline       StructureController* GetController()       { return m_pController; }
            inline const StructureStorage*    GetStorage()    const { return m_pStorage; }
            inline       StructureStorage*    GetStorage()          { return m_pStorage; }
            inline const StructureTerminal*   GetTerminal()   const { return m_pTerminal; }
            inline       StructureTerminal*   GetTerminal()         { return m_pTerminal; }

        private:
            js::val     m_js_obj;

            std::string m_name;
            RoomFlags   m_flags;

            StructureArray       m_structures;
            StructureController* m_pController = nullptr;
            StructureStorage*    m_pStorage    = nullptr;
            StructureTerminal*   m_pTerminal   = nullptr;

            ConstructionSiteArray m_csites;

            // TODO: This will likely never change. Should be moved to global heap.
            mutable std::optional<Terrain>      m_terrain  = std::nullopt;
            mutable std::optional<SourceArray>  m_sources  = std::nullopt;
            mutable std::optional<MineralArray> m_minerals = std::nullopt;
    };

    inline void Room::AddConstructionSite(const js::val& _js_csite)
    {
        m_csites.emplace_back(*this, _js_csite);
    }

    inline const Room::StructureArray& Room::GetStructures() const 
    {
        return m_structures;
    }

    inline const Room::ConstructionSiteArray& Room::GetConstructionSites() const
    {
        return m_csites;
    }

    inline const Terrain& Room::GetTerrain() const
    {
        if (!m_terrain)
            m_terrain = Terrain(m_js_obj.call<js::val>("getTerrain"));

        return *m_terrain;
    }

    inline const std::vector<EnergySource>& Room::GetSources() const
    {
        if (m_sources)
            return *m_sources;
        
        // Find all energy sources in the room
        js::val js_sources_obj = m_js_obj.call<js::val>("find", utils::constants::FIND_SOURCES);
        js::array_native js_sources;
        js::js_array_to_vector(js_sources_obj, js_sources);

        SourceArray energySources;
        energySources.reserve(js_sources.size());
        for (const auto& js_source : js_sources)
        {            
            energySources.emplace_back(*this, js_source);
        }

        m_sources = std::move(energySources);
        return *m_sources;
    }

    inline const std::vector<Mineral>& Room::GetMinerals() const
    {
        // Find all minerals in the room
        js::val js_minerals_obj = m_js_obj.call<js::val>("find", utils::constants::FIND_MINERALS);
        js::array_native js_minerals;
        js::js_array_to_vector(js_minerals_obj, js_minerals);

        MineralArray mineralSources;
        mineralSources.reserve(js_minerals.size());
        for (const auto& js_source : js_minerals)
        {            
            mineralSources.emplace_back(*this, js_source);
        }

        m_minerals = std::move(mineralSources);
        return *m_minerals;
    }
}
