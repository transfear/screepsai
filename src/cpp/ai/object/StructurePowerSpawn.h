#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructurePowerSpawn : public OwnedStructure
    {
        public:
            inline StructurePowerSpawn(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::POWER_SPAWN) { }

        protected:

    };
}
