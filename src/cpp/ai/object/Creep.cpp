#include "Creep.h"

using namespace ai::utils::constants;

namespace ai::object
{
    const CreepBody& Creep::GetBody() const
    {
        if (m_body.isValid)
            return m_body;

        // Lazy evaluation
        const js::val js_body  = m_js_obj["body"];
        const uint8_t numParts = js_body["length"].as<uint8_t>();
        m_body.numParts = static_cast<uint8_t>(numParts);

        for (unsigned int i = 0; i < numParts; ++i)
        {
            js::val js_bodyPart = js_body[i];
            
            const uint8_t hp = js_bodyPart["hits"].as<uint8_t>();
            m_body.hp[i] = hp;

            const int   bodyPartIdx = std::find(BODYPART_NAME.begin(), BODYPART_NAME.end(), js_bodyPart["type"].as<std::string>()) - BODYPART_NAME.begin();
            const BODYPART bodyPart = static_cast<BODYPART>(bodyPartIdx);
            
            m_body.parts[i] = bodyPart;
            if (hp > 0)
                ++m_body.validParts[bodyPart];
        }

        m_body.isValid = true;
        return m_body;
    }
}
