#pragma once

#include "RoomObject.h"
#include "ai/utils/Constants.h"

namespace ai::object
{
    class ConstructionSite : public RoomObject
    {
        public:
            inline ConstructionSite(const Room& _room, const js::val& _js_obj) 
            : RoomObject(_js_obj, _room)
            {
                const auto structureNameIt = std::find(ai::utils::constants::STRUCTURE_NAME.begin(), ai::utils::constants::STRUCTURE_NAME.end(), _js_obj["structureType"].as<std::string>());
                m_type = static_cast<ai::utils::constants::STRUCTURE>(structureNameIt - ai::utils::constants::STRUCTURE_NAME.begin());
            }

            inline ai::utils::constants::STRUCTURE GetType() const { return m_type; }

        protected:
            ai::utils::constants::STRUCTURE m_type;
    };
}
