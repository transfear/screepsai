#pragma once

#include "StructureBase.h"

namespace ai::object
{
    class StructurePortal : public StructureBase
    {
        public:
            inline StructurePortal(Room& _room, const js::val& _js_obj) : StructureBase(_room, _js_obj, ai::utils::constants::STRUCTURE::PORTAL) { }

        protected:

    };
}
