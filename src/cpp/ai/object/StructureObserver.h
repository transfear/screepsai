#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureObserver : public OwnedStructure
    {
        public:
            inline StructureObserver(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::OBSERVER) { }

        protected:

    };
}
