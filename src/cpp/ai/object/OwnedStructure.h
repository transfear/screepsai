#pragma once

#include "StructureBase.h"

namespace ai::object
{
    class OwnedStructure : public StructureBase
    {
        public:
        
            inline OwnedStructure(Room& _room, const js::val& _js_obj, const ai::utils::constants::STRUCTURE& _type); 

            inline bool IsMine() const { return m_isMine; }
            inline const std::string& GetOwner() const { return m_owner; }

        protected:

            std::string m_owner;
            bool        m_isMine;

    };

    inline OwnedStructure::OwnedStructure(Room& _room, const js::val& _js_obj, const ai::utils::constants::STRUCTURE& _type)
    : StructureBase(_room, _js_obj, _type), m_isMine(_js_obj["my"].as<bool>())
    {
         const js::val& js_owner = _js_obj["owner"];
         if (!js_owner.isNull() && !js_owner.isUndefined())
         {
             m_owner = js_owner["username"].as<std::string>();
         }
    }
}
