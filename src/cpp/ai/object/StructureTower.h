#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureTower : public OwnedStructure
    {
        public:
            inline StructureTower(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::TOWER) { }

        protected:

    };
}
