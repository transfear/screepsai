#pragma once

#include "StructureBase.h"

namespace ai::object
{
    class StructureContainer : public StructureBase
    {
        public:
            inline StructureContainer(Room& _room, const js::val& _js_obj) : StructureBase(_room, _js_obj, ai::utils::constants::STRUCTURE::CONTAINER) { }

        protected:

    };
}
