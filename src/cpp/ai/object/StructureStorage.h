#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureStorage : public OwnedStructure
    {
        public:
            inline StructureStorage(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::STORAGE) { }

        protected:

    };
}
