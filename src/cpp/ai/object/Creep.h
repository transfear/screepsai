#pragma once

#include <string>
#include "cppreeps/cppreeps.h"
#include "ai/utils/Constants.h"
#include "RoomObject.h"

namespace ai::object
{
    class Room;

    enum class CreepFlags : uint8_t
    {
        kNone   = 0x00,
        kMine   = 0x01,
        kAllied = 0x02,
        kEnemy  = 0x04
    };

    inline CreepFlags operator&(CreepFlags a, CreepFlags b)
    {
        return static_cast<CreepFlags>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
    }

    inline CreepFlags operator|(CreepFlags a, CreepFlags b)
    {
        return static_cast<CreepFlags>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
    }

    template <CreepFlags toTest>
    inline bool TestFlag(CreepFlags _flags)
    {
        return (_flags & toTest) != CreepFlags::kNone;
    }

    struct CreepBody
    {
        std::array<utils::constants::BODYPART, utils::constants::MAX_CREEP_SIZE> parts;
        std::array<uint8_t,utils::constants::MAX_CREEP_SIZE> hp;
        uint8_t numParts = 0;
        bool    isValid  = false;

        std::array<uint8_t, utils::constants::BODYPART::BODYPART_COUNT> validParts = {0,0,0,0,0,0,0,0}; // per type, hp > 0
    };

    class Creep : public RoomObject
    {
        public:
            using BodyPartArray = std::array<uint8_t, utils::constants::BODYPART::BODYPART_COUNT>;
            
            Creep(const js::val& _jsObj, const std::string& _id, const CreepFlags& _flags, const Room& _roomObj) : RoomObject(_jsObj, _roomObj), m_id(_id), m_flags(_flags) { }

            inline const Room&      GetRoom() const { return m_room; }
            inline const std::string& GetId() const { return m_id; }
            inline      CreepFlags GetFlags() const { return m_flags; }

            const CreepBody& GetBody() const;   // lazy evaluation TODO: use std::optional?
        private:

            std::string m_id;
            CreepFlags  m_flags;

            mutable CreepBody m_body;
            uint8_t m_CreepPad[3];  // unused
    };

}
