#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureController : public OwnedStructure
    {
        public:
            inline StructureController(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::CONTROLLER) { }

        protected:

    };
}
