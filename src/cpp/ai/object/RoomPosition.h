#pragma once

#include <cstdint>
#include "cppreeps/cppreeps.h"

namespace ai::object
{
    struct RoomPosition
    {
        inline RoomPosition(const RoomPosition& _o) { m.signature = _o.m.signature; }
        inline RoomPosition(uint8_t _x, uint8_t _y) {  m.p.x = _x; m.p.y = _y; }
        inline RoomPosition(const js::val& _js_pos) : RoomPosition(_js_pos["x"].as<uint8_t>(), _js_pos["y"].as<uint8_t>()) { }

        inline void Set(uint8_t _x, uint8_t _y) { m.p.x = _x; m.p.y = _y; }
        inline void Set(const js::val& _js_pos) { Set(_js_pos["x"].as<uint8_t>(), _js_pos["y"].as<uint8_t>()); }

        inline uint16_t GetSignature() const { return m.signature; }
        inline uint8_t  GetX() const { return m.p.x; }
        inline uint8_t  GetY() const { return m.p.y; }

        union
        {
            struct 
            {
                uint8_t x;
                uint8_t y;       
            } p;

            uint16_t signature;
        } m;
    };
}
