#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureExtension : public OwnedStructure
    {
        public:
            inline StructureExtension(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::EXTENSION) { }

        protected:

    };
}
