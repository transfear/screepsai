#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureLink : public OwnedStructure
    {
        public:
            inline StructureLink(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::LINK) { }

        protected:

    };
}
