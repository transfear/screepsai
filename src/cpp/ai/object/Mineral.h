#pragma once

#include "RoomObject.h"

namespace ai::object
{

    class Mineral : public RoomObject
    {
        public:
            inline Mineral(const Room& _room, const js::val& _jsObj) : RoomObject(_jsObj, _room) { }

        protected:
            uint8_t m_pad[2];   // unused
    };
}
