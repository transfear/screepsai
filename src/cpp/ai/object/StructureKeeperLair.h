#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureKeeperLair : public OwnedStructure
    {
        public:
            inline StructureKeeperLair(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::KEEPER_LAIR) { }

        protected:

    };
}
