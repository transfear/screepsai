#pragma once

#include "StructureBase.h"

namespace ai::object
{
    class StructureWall : public StructureBase
    {
        public:
            inline StructureWall(Room& _room, const js::val& _js_obj) : StructureBase(_room, _js_obj, ai::utils::constants::STRUCTURE::WALL) { }

        protected:

    };
}
