#pragma once

#include <array>
#include <string>
#include "cppreeps/cppreeps.h"
#include "ai/utils/Constants.h"

namespace ai::object
{
    template <typename TileT = uint8_t>
    class TerrainTemplate
    {
        public:
            using Tile = TileT;
            static constexpr Tile UNPATHABLE = std::numeric_limits<Tile>::max();

            inline TerrainTemplate() { }
            inline TerrainTemplate(Tile _fillValue);
            inline TerrainTemplate(const js::val& _terrainObj);
            inline Tile Get(int x, int y) const { return m_terrain[y][x]; }
            inline void Set(int x, int y, Tile value) { m_terrain[y][x] = value; }

            template <class Func>
            inline void for_each(Func &&f);
            template <class Func>
            inline void for_each(Func &&f) const;

        private:
            std::array<std::array<Tile, utils::constants::ROOM_WIDTH>, utils::constants::ROOM_HEIGHT> m_terrain;
    };

    using Terrain         = TerrainTemplate<uint8_t>;
    using DistanceTerrain = TerrainTemplate<uint16_t>;

    template <typename Tile>
    inline TerrainTemplate<Tile>::TerrainTemplate(const js::val& _terrainObj)
    {
        js::val js_rawTerrain = _terrainObj.call<js::val>("getRawBuffer");
       
        int readOffset = 0;
        for (auto& row : m_terrain)
        {
            for (Tile& buf : row)
            {
                buf = js_rawTerrain[readOffset++].as<Tile>();
            }
        }
    }

    template <typename Tile>
    inline TerrainTemplate<Tile>::TerrainTemplate(Tile _fillValue)
    {
        for (auto& row : m_terrain)
        {
            for (Tile& buf : row)
            {
                buf = _fillValue;
            }
        }        
    }

    template <typename Tile>
    template <class Func>
    inline void TerrainTemplate<Tile>::for_each(Func &&f)
    {
        for (uint8_t y = 0; y < utils::constants::ROOM_HEIGHT; ++y)
        {
            for (uint8_t x = 0; x < utils::constants::ROOM_WIDTH; ++x)
            {
                f(x, y, m_terrain[y][x]);
            }
        }
    }

    template <typename Tile>
    template <class Func>
    inline void TerrainTemplate<Tile>::for_each(Func &&f) const
    {
        for (uint8_t y = 0; y < utils::constants::ROOM_HEIGHT; ++y)
        {
            for (uint8_t x = 0; x < utils::constants::ROOM_WIDTH; ++x)
            {
                f(x, y, m_terrain[y][x]);
            }
        }
    }
}
