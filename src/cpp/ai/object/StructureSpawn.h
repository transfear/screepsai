#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureSpawn : public OwnedStructure
    {
        public:
            inline StructureSpawn(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::SPAWN) { }

        protected:

    };
}
