#pragma once

#include "RoomObject.h"
#include "ai/utils/Constants.h"

namespace ai::object
{
    class StructureBase : public RoomObject
    {
        public:
            inline StructureBase(const Room& _room, const js::val& _js_obj, const ai::utils::constants::STRUCTURE& _type) : RoomObject(_js_obj, _room), m_type(_type) { }
            inline virtual ~StructureBase() { }

            inline ai::utils::constants::STRUCTURE GetType() const { return m_type; }

        protected:
            
            ai::utils::constants::STRUCTURE m_type;
            
            uint8_t m_pad[2];   // unused
    };
}
