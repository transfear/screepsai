#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureLab : public OwnedStructure
    {
        public:
            inline StructureLab(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::LAB) { }

        protected:

    };
}
