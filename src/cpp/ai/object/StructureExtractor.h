#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureExtractor : public OwnedStructure
    {
        public:
            inline StructureExtractor(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::EXTRACTOR) { }

        protected:

    };
}
