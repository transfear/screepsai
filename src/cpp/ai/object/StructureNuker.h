#pragma once

#include "OwnedStructure.h"

namespace ai::object
{
    class StructureNuker : public OwnedStructure
    {
        public:
            inline StructureNuker(Room& _room, const js::val& _js_obj) : OwnedStructure(_room, _js_obj, ai::utils::constants::STRUCTURE::NUKER) { }

        protected:

    };
}
