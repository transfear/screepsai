#pragma once

#include "StructureBase.h"

namespace ai::object
{
    class StructureRoad : public StructureBase
    {
        public:
            inline StructureRoad(Room& _room, const js::val& _js_obj) : StructureBase(_room, _js_obj, ai::utils::constants::STRUCTURE::ROAD) { }

        protected:

    };
}
