#include "RoomObject.h"
#include "Room.h"

namespace ai::object
{
    RoomObject::RoomObject(const js::val& _js_obj, const Room& _room)
    : m_room(_room), m_js_obj(_js_obj), m_pos(_js_obj["pos"])
    {

    }
}
