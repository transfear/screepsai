#pragma once

#include "StructureBase.h"

namespace ai::object
{
    class StructurePowerBank : public StructureBase
    {
        public:
            inline StructurePowerBank(Room& _room, const js::val& _js_obj) : StructureBase(_room, _js_obj, ai::utils::constants::STRUCTURE::POWER_BANK) { }

        protected:

    };
}
