#pragma once

#include <optional>

#include "cppreeps/cppreeps.h"
#include "RoomObject.h"

namespace ai::object
{
    class EnergySource : public RoomObject
    {
        public:
            inline EnergySource(const Room& _room, const js::val& _jsObj) : RoomObject(_jsObj, _room), m_capacity(_jsObj["energyCapacity"].as<uint16_t>()) { }

            uint8_t GetHarvestableLocations();
            inline uint16_t GetCapacity() const { return m_capacity; }

        private:

            uint16_t m_capacity;

            std::optional<uint8_t> m_harvestableLocations = std::nullopt;   // Lazily fetched

            uint8_t m_pad[2];   // unused
    };
}
