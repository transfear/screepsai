#include <emscripten.h>
#include <emscripten/val.h>
#include <emscripten/bind.h>

#include "cppreeps/cppreeps.h"
#include "cppreeps/lzw.h"

#include "ai/Global.h"
#include "ai/event/Reconciler.h"
#include "ai/mem/GlobalCache.h"

#include "ai/dbg/Assert.h"

#include "ai/roomplan/Planner.h"

struct FrameProfiler
{
    FrameProfiler(ai::Global& _gbl) : m_gbl(_gbl) { }
    ~FrameProfiler()
    {
        javascript::val& game = screeps::tick->Game;
        javascript::val  cpu  = game["cpu"];

        const int tickId   = game["time"].as<int>();
        
        const int   cpuLimit = cpu["tickLimit"].as<int>();
        const int   bucket   = cpu["bucket"].as<int>();
        const float usedCPU  = cpu.call<float>("getUsed");
        
        PROF_LOG(m_gbl.dbg, "Tick %d: CPU (%.2f/%d), Bucket %d", tickId, usedCPU, cpuLimit, bucket);
    }

    ai::Global& m_gbl;
};

extern "C"
{
    void native_loop()
    {
        screeps::Init();

        const bool needsReload = ai::mem::GlobalCache::Init();  // In case a global heap reset happens

        ai::Global gbl;
        FrameProfiler fp(gbl);

        if (needsReload)
            gbl.mem.Load(gbl);

        gbl.FixupPointers();

        // Analyze stuff that is new this frame, and push events
        {
            ai::event::Reconciler reconciler(gbl.eventMgr, gbl.gs, gbl.mem);
            reconciler.Reconcile(); 
        }

        // Process pushed events
        gbl.eventMgr.ProcessEvents(gbl);

        // Temp, test planner
        ai::roomplan::Planner roomPlanner(screeps::tick->Game["time"].as<uint32_t>());
        //ai::roomplan::Planner roomPlanner(0);
        roomPlanner.PlanAllRooms(gbl);

        js::object_native creepsMap;
        js::js_object_to_map(screeps::tick->Game["creeps"], creepsMap);
        for (auto const& kv : creepsMap)
        {
            auto const& name  = kv.first;
            auto const& creep = kv.second;
            creep.call<int>("say", name);
        }

        gbl.mem.Save(gbl);
    }
}

EMSCRIPTEN_BINDINGS(native_loop)
{
    emscripten::function("native_loop", &native_loop);
}

