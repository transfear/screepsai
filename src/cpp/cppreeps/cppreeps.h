#pragma once

#include <algorithm>
#include <cstdio>
#include <memory>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <utility>

#include <emscripten.h>
#include <emscripten/val.h>
#include <emscripten/bind.h>

namespace javascript {
    
    using val = emscripten::val;
    
    using array_native       = std::vector<val>;
    using object_native      = std::unordered_map<std::string, val>;
    using object_keys_native = std::unordered_set<std::string>;
    
    extern const val gGlobal;   // Global JS scope
    extern const val gObject;   // Global JS Object
    
    inline val get_global(char const* name) { return val::global(name); }
    
    inline val get_global(std::string const& name) { return val::global(name.c_str()); }
    
    template <class T>
    inline val gCONST(T&& name) { return val::global(std::forward<T>(name)); }
    
    inline void js_array_to_vector(val const& inArray, array_native& outArray) 
    {
        const int size = inArray["length"].as<int>();
        outArray.reserve(static_cast<size_t>(size));
        for (int i = 0; i < size; ++i)
            outArray.emplace_back(inArray[i]);
    }
    
    inline void js_object_to_map(val const& inObj, object_native& outObj)
    {
        const val keys = gObject.call<val>("keys", inObj);
        const int size = keys["length"].as<int>();
        
        for (int i = 0; i < size; ++i)
        {
            auto key = keys[i].as<std::string>();
            outObj.emplace_hint(outObj.end(), key, inObj[key]);
        }
    }

    inline void js_object_keys_to_set(val const& inObj, object_keys_native& outSet)
    {
        const val keys = gObject.call<val>("keys", inObj);
        const int size = keys["length"].as<int>();

        outSet.reserve(static_cast<size_t>(size));
        for (int i = 0; i < size; ++i)
        {
            auto key = keys[i].as<std::string>();
            outSet.emplace_hint(outSet.end(), key);
        }
    }
}

namespace js = javascript;

namespace screeps
{       
    struct tick_t
    {
        javascript::val Game             = javascript::get_global("Game");
        javascript::val InterShardMemory = javascript::get_global("InterShardMemory");
        javascript::val Memory           = javascript::get_global("Memory");
        javascript::val RawMemory        = javascript::get_global("RawMemory");
        javascript::val PathFinder       = javascript::get_global("PathFinder");
        javascript::val Constants        = javascript::get_global("Constants");
        
         //tick_t() { std::printf("TICK{%d} CONSTRUCTED\n", Game["time"].as<int>()); }
        //~tick_t() { std::printf("TICK{%d} DESTROYED\n",   Game["time"].as<int>()); }
    };
    
    extern std::unique_ptr<tick_t> tick;
    
    /// Performs recaching of global game objects,
    /// resets previous tick data reference counters, releases objects for GC
    inline void Init() { tick.reset(new tick_t()); }
}
