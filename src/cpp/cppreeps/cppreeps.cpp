#include "cppreeps.h"

namespace javascript
{
    const val gGlobal = val::global();           // Global JS scope
    const val gObject = val::global("Object");   // Global JS Object
}

namespace screeps
{
    std::unique_ptr<tick_t> tick = nullptr;
}
