#pragma once

/// Header file contains Emscripten bindings of lzw.hpp:
///  - std::wstring lzw_encode(std::wstring in);
///  - std::wstring lzw_decode(std::wstring in);

#include <emscripten.h>
#include <emscripten/val.h>
#include <emscripten/bind.h>

/// Standalone hpp implementation file (lib/...)
#include "../lzw/lzw.h"

namespace screeps
{
    class binary_to_UTF16 : public lzw::details::lzw_codec
    <
        lzw::dictionaries::BINARY_256_common,
        lzw::dictionaries::UTF16_pack
    > {};

    inline std::wstring lzw_encode(const std::wstring& in)
    {
        using codec = lzw::codecs::string_to_UTF16;
        
        std::wstring out;
        out.reserve(in.size());
        codec::encode(in.begin(), in.end(), std::back_inserter(out));
        return out;
    }

    inline std::wstring lzw_encode_binary(const std::vector<unsigned char>& in)
    {
        using codec = binary_to_UTF16;
        
        std::wstring out;
        out.reserve(in.size());
        codec::encode(in.begin(), in.end(), std::back_inserter(out));
        return out;
    }

    inline std::wstring lzw_decode(const std::wstring& in)
    {
        using codec = lzw::codecs::string_to_UTF16;
        
        std::wstring out;
        out.reserve(in.size()*2);
        codec::decode(in.begin(), in.end(), std::back_inserter(out));
        return out;
    }

    inline void lzw_decode_binary(const std::wstring& in, std::vector<unsigned char>& out)
    {
        using codec = binary_to_UTF16;
        
        out.reserve(in.size()*2);
        codec::decode(in.begin(), in.end(), std::back_inserter(out));
    }



    //EMSCRIPTEN_BINDINGS(lzw)
    //{
    //    emscripten::function("lzw_encode", &lzw_encode);
    //    emscripten::function("lzw_decode", &lzw_decode);
    //}
    
}